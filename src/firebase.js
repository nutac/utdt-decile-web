// src/firebase.js
import { initializeApp } from "firebase/app";
import {
    getDatabase, ref, get, set, push,
    query, orderByChild, equalTo, remove,
} from "firebase/database";


const firebaseConfig = {
    apiKey: "AIzaSyBMmYgZfwcy4kMdRZ8nH_zEfsHNOCh8koU",
    authDomain: "picr-40137.firebaseapp.com",
    databaseURL: "https://picr-40137-default-rtdb.firebaseio.com",
    projectId: "picr-40137",
    storageBucket: "picr-40137.appspot.com",
    messagingSenderId: "946678339287",
    appId: "1:946678339287:web:7529255941295a774f0292"
};
 
const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
 
// export { database, ref, get };
export async function getRegistros() {
    const registrosRef = ref(database, 'utdtSenalar/registros');
    try {
        const snapshot = await get(registrosRef);
        if (snapshot.exists()) {
            return snapshot.val();
        } else {
            console.log("No data available");
            return null;
        }
    } catch (error) {
        console.error("Error fetching registros: ", error);
    }
}
export async function pushNewRegistro(registro) {
    const folderRef = ref(database, 'utdtSenalar/registros');
    try {
        // await set(testRef, "prueba");
        await push(folderRef, registro)
        // console.log("Data saved successfully");
        return "ok"
    } catch (error) {
        console.error("Error saving data: ", error);
        return error
    }
}

export async function saveTest() {
    const testRef = ref(database, 'utdtSenalar/test2');
    try {
        await set(testRef, "prueba");
        // console.log("Data saved successfully");
        return "ok"
    } catch (error) {
        console.error("Error saving data: ", error);
        return error
    }
}
export async function deleteRegistroByDateAndTime(_date, _time) {
    // const registrosRef = child(dbRef, "utdt/registros");
    const folderRef = ref(database, 'utdtSenalar/registros');

    const q = query(folderRef, orderByChild('fecha'), equalTo(_date));

    // Obtener los datos que coinciden con la fecha
    const snapshot = await get(q);

    if (snapshot.exists()) {
        snapshot.forEach(childSnapshot => {
            // Revisar si el campo 'hora' también coincide
            if (childSnapshot.val().hora === _time) {
                // Eliminar el registro
                remove(childSnapshot.ref)
                    .then(() => {
                        console.log(`Registro con fecha ${_date} y hora ${_time} eliminado.`);
                    })
                    .catch(error => {
                        console.error("Error al eliminar el registro: ", error);
                    });
            }
        });
    } else {
        console.log("No se encontraron registros con esa fecha.");
    }
}
