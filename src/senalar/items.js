export let listas = {};
export async function loadList() {
    const response = await fetch('/data/data/senalar/tarea_senalar.csv');
    const csvData = await response.text();

    let items = parseCSV(csvData);
    let lists = items.reduce((acc, item) => {
        let listName = item.Lista || 'sin_lista'; // Asignar 'sin_lista' si el campo está vacío
        if (!acc[listName]) acc[listName] = [];
        acc[listName].push(item);
        return acc;
    }, {});

    // listas = Object.keys(lists).map((key) => ({
    //     name: key,
    //     items: lists[key],
    // }));
    listas = lists
}


function parseCSV(csv) {
    csv = csv.replace(/\r/g, '');
    let lines = csv.trim().split('\n');
    // const headers = lines[0].split(',').map(header => header.trim());
    // let headers = lines.shift().split(',').map(header => header.trim());
    let headers = lines.shift().split(',').map(header => header.trim());

    return lines.map(line => {
        let data = line.split(',');
        let obj = {};
        headers.forEach((header, index) => {
            obj[header] = data[index].trim();
        });
        return obj;
    });
}
