
const DEBUG_ASSETS = false
export const assets = {};
window.assets = assets
// export const fondoImages = [];
const path_fondos = '/assets/ilustraciones/Fondos/';
export const fondosImgNames = [


    'fondo_1.png',
    'fondo_1_blur.png',
    'fondo_2.png',
    'fondo_2_blur.png',
    'fondo_3.png',
    'fondo_3_blur.png',
    'fondo_4.png',
    'fondo_4_blur.png',
    'fondo_5.png',
    'fondo_5_blur.png',
]
// export const personajePoses = [];
const path_personaje_pose = '/assets/ilustraciones/Personaje/';
export const poseImgNames = [
    'exploradora_festejando.png',
    'exploradora_muestra_dibujos_cuerpo.png',
    'exploradora_pausa.png',
    'exploradora_saludando.png',
    'exploradora_senala_derecha_cuerpo.png',
    'exploradora_senala_izquierda_cuerpo.png',
    'exploradora_transicion.png'
]
// export const personajeBocas = [];
const path_personaje_bocas = '/assets/ilustraciones/Personaje/Expresiones/';
export const bocaImgNames = [
    "expresion1.png",
    "expresion14.png",

    "expresion3.png",
    "expresion7.png",
    "expresion10.png",
    "expresion13.png",
    "expresion14.png"
]

export const ojosImgNames = [
    '/assets/ilustraciones/Personaje/exploradora_muestra_dibujos_cara.png',
    '/assets/ilustraciones/Personaje/exploradora_senala_izquierda_cara.png',
    '/assets/ilustraciones/Personaje/exploradora_muestra_dibujos_cara.png',
    '/assets/ilustraciones/Personaje/exploradora_senala_derecha_cara.png',
]

// const path_dibujos = '/senalar/dibujos/';
const path_dibujos = '/data/data/senalar/dibujos/';

export const tutorialImages = [];
// export const path_oraciones = '/senalar/audios/oraciones/';
export const path_oraciones = '/data/data/senalar/oraciones/';

// export const tutorialAudios = [];

let assetsLoaded;

export let extras = {}

// const path_audios_personaje = '/assets/audios/audios_personaje/';
const path_audios_personaje = '/senalar/audios/personaje/';

export const audiosPersonajeNames = [
    "Presentacion_a.mp3",
    "Presentacion_b.mp3",
    "Presentacion_c.mp3",
    "Presentacion_d.mp3",
    "Instrucciones1mono_a.mp3",
    "Instrucciones1mono_b.mp3",
    "Instrucciones1mono_c.mp3",
    "Instrucciones2mono_a.mp3",
    "Instrucciones2mono_b.mp3",
    "Instrucciones3mono_a.mp3",
    "Instrucciones3mono_b.mp3",
    "Instrucciones3mono_c.mp3",
    "Instrucciones4mono_a.mp3",
    "Instrucciones4mono_b.mp3",
    "Instrucciones5mono_a.mp3",
    "Instrucciones5mono_b.mp3",
    "Instrucciones6mono_a.mp3",
    "Instrucciones6mono_b.mp3",
    "Instrucciones7mono_a.mp3",
    "Instrucciones7mono_b.mp3",
    "Instrucciones7mono_c.mp3",
    "Instrucciones8mono_a.mp3",
    "Instrucciones8mono_b.mp3",
    "Instrucciones9mono_a.mp3",
    "Instrucciones9mono_b.mp3",
    "Instrucciones10mono_a.mp3",
    "Instrucciones10mono_b.mp3",
    "Instrucciones11mono_a.mp3",
    "Instrucciones11mono_b.mp3",
    "Instrucciones11mono_c.mp3",

    "Practica1_vamos_a_pacticar.mp3",
    "Practica2_Toca_al_personaje_correcto.mp3",
    "Practica3_intentalo_de_vuelta.mp3",
    "Practica4_Este_es_el_personaje_correcto.mp3",
    "Practica5_escucha_de_nuevo.mp3",
    "Practica6_estas_ahi_Toca_a.mp3",
    "Practica6_estas_ahi_Toca_b.mp3",
    "Practica7_a.mp3",
    "Practica7_b.mp3",
    "Transicion_a.mp3",
    "Transicion_b.mp3",
    "Transicion_c.mp3",
    "Transicion_d.mp3",
    "Prueba_pasemos_al_siguiente.mp3",
    "Msj_motivacional_1.mp3",
    "Msj_motivacional_2.mp3",
    "Msj_motivacional_3.mp3",
    "Msj_motivacional_4.mp3",
    "Pausa_1_a.mp3",
    "Pausa_1_b.mp3",
    "Pausa_1_c.mp3",
    "Pausa_1_d.mp3",
    "Pausa_2_a.mp3",
    "Pausa_2_b.mp3",
    "Pausa_2_c.mp3",
    "Pausa_2_d.mp3",
    "Msj_final_a.mp3",
    "Msj_final_b.mp3",
    "Msj_final_c.mp3",

]

export function preloadAssets(p, listItems) {
    assetsLoaded = 0;

    let totalAssets =
        // listItems.length * 2+
        tutorialItems.length * 2
        + fondosImgNames.length
        + poseImgNames.length
        + bocaImgNames.length
        // + audiosPersonajeNames.length
        + ojosImgNames.length
        ;
    if (!DEBUG_ASSETS) {
        totalAssets += listItems.length * 2
        listItems.forEach(item => {
            let imageName = `${item.Nombre_dibujo}.png`;
            // console.log(path_dibujos + imageName)
            p.loadImage(path_dibujos + imageName, img => {
                assets[imageName] = img;
                assetsLoaded++;
                updateProgress(totalAssets, assetsLoaded);
            });

            let audioName = processAudioFileName(item.oracion);
            p.loadSound(path_oraciones + audioName, audio => {
                assets[audioName] = audio;
                assetsLoaded++;
                updateProgress(totalAssets, assetsLoaded);
            });
        });
    }

    tutorialItems.forEach(item => {
        let imageName = `${item.Nombre_dibujo}.png`;
        p.loadImage(path_dibujos + imageName, img => {
            assets[imageName] = img;
            assetsLoaded++;
            updateProgress(totalAssets, assetsLoaded);
        });

        let audioName = processAudioFileName(item.oracion);
        p.loadSound(path_oraciones + audioName, audio => {
            assets[audioName] = audio;
            assetsLoaded++;
            updateProgress(totalAssets, assetsLoaded);
        });
    });

    // Cargar fondos
    for (let i = 0; i < fondosImgNames.length; i++) {
        p.loadImage(path_fondos + fondosImgNames[i], img => {
            assets[fondosImgNames[i]] = img;
            assetsLoaded++;
            updateProgress(totalAssets, assetsLoaded);
        });
    }

    // Cargar poses del personaje
    for (let i = 0; i < poseImgNames.length; i++) {
        p.loadImage(path_personaje_pose + poseImgNames[i], img => {
            assets[poseImgNames[i]] = img;
            assetsLoaded++;
            updateProgress(totalAssets, assetsLoaded);
        });
    }

    // Cargar bocas del personaje
    for (let i = 0; i < bocaImgNames.length; i++) {
        p.loadImage(path_personaje_bocas + bocaImgNames[i], img => {
            assets[bocaImgNames[i]] = img;
            assetsLoaded++;
            updateProgress(totalAssets, assetsLoaded);
        });
    }

    // Cargar ojos del personaje
    for (let i = 0; i < ojosImgNames.length; i++) {
        p.loadImage(ojosImgNames[i], img => {
            assets[ojosImgNames[i]] = img;
            assetsLoaded++;
            updateProgress(totalAssets, assetsLoaded);
        });
    }

    // p.loadSound('/assets/audios/Musica_inicio_639933__sergmusic__blue-sky_CORTADO.mp3', audio => {
    //     extras['musica_inicio'] = audio
    // })

    /// EXTRAS /////////////////////////////
    extras['musica_inicio'] = '/assets/audios/Musica_inicio_639933__sergmusic__blue-sky_CORTADO.mp3'
    extras['musica_motivacional'] = '/assets/audios/Msj_motiv_1_341984__unadamlar__winning_CORTADO.mp3'
    extras['musica_final'] = '/assets/audios/Final1_36161__sagetyrtle__bells2_CORTADO.mp3'


    p.loadImage(
        '/assets/ilustraciones/Otros/emoji_positivo.png', img => {
            extras['emoji_positivo'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/emoji_neutro.png', img => {
            extras['emoji_neutro'] = img
        })
    p.loadImage(
        // '/assets/ilustraciones/Otros/estrella_b2.png', img => {
        '/assets/ilustraciones/Otros/estrella_b.png', img => {
            extras['estrella'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/boton_play1.png', img => {
            extras['boton_play1'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/boton_play2.png', img => {
            extras['boton_play2'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/contador_estrellas_1.png', img => {
            extras['contador_estrellas_1'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/contador_estrellas_2.png', img => {
            extras['contador_estrellas_2'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/contador_estrellas_3.png', img => {
            extras['contador_estrellas_3'] = img
        })
    p.loadImage(
        '/assets/ilustraciones/Otros/confeti_globos.png', img => {
            extras['confeti'] = img
        })

    extras['silbido'] = '/assets/audios/silbido_llamado_atencion.mp3'


    // Cargar AUDIOS del personaje
    for (let i = 0; i < audiosPersonajeNames.length; i++) {
        // p.loadSound(path_audios_personaje + audiosPersonajeNames[i], audio => {
        //     assets[audiosPersonajeNames[i]] = audio;
        //     assetsLoaded++;
        //     updateProgress(totalAssets, assetsLoaded);
        // });
        extras[audiosPersonajeNames[i]] = path_audios_personaje + audiosPersonajeNames[i]
    }

}

export function processAudioFileName(oracion) {
    return oracion
        .replace(/[¿?]/g, '')
        .replace(/ /g, '_')
        .normalize('NFD').replace(/[\u0300-\u036f]/g, '')
        .concat('.mp3');
}

/////////////////////////////////
import * as dom from '../dom-helpers.js';
import { tutorialItems } from './state.js';
let progressBar = document.querySelector("#progressBar");
let progressContainer = document.querySelector("#containerProgress");

export function updateProgress(total, assetsLoaded) {
    let percent = (assetsLoaded / total) * 100;
    progressBar.style.width = percent + "%";

    dom.addClass(progressContainer, 'show');
    if (percent === 100) {
        dom.removeClass(progressContainer, 'show');
    }
}