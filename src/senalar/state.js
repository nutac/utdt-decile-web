import { sayOracion, playAudio } from './audio.js';
import { pausar } from './utils.js';
import * as assets from './assets.js';
import * as utils from './utils.js';
import { DEBUG } from './senalar.js';
import * as senalar from './senalar.js';


import * as dom from '../dom-helpers.js'
import { playSecuenciaAudio, pauseAudio } from './audio.js';

import * as helpers from './state_helpers.js'
import * as fb from '../firebase.js'

/*
notas:
-ocultar imagend e la cebra cuadno dice "a veces no es pregunta"
    -que el area del dibujo sea blanca
-en la pintora, señana para el otro lado

*/

export let formData = {};
export let registroGeneral = {};

export let isTutorial;
export let currentIndex;
export let listItems = [];
export let currentItem;

export let tutorialItems = [];

export let currentFondo;

export let drawPersonaje;
export let boca = {
    counter: 0,
    maxCounter: 3,
    animationIndex: 0,
    animar: false,
    current: '',
    silbando: false

}
export let ojos = {
    counter: 0,
    maxCounter: 10,
    animationIndex: 0,
    animar: false,
    current: ''
}
export let currentPose;

export let showFeedback;
export const NEUTRO = 0
export const POSITIVO = 1
export let feedback = POSITIVO
// export let messageText = '';
export let messageColor = '';

export let attentionTimer;

export let isAwaitingInteraction;
export let drawCortina;
// export let areaDibujo = { x: 0, y: 0, w: 0, h: 0, inflexion1: 0, inflexion2: 0 };
let startTime;


let duracionMostrarDibujo = 1000
let timerEsperaMax

let darExplicacion = false;
export const NO = 0
export const IZQ = 1
export const DER = 2
export let titilar = NO

export let hideImage;

let contadorIncorrectas;

export let timerCount;
// let responseTime = 0;

export let expPausada;

export let motivacional
export let pausaDescanso
export let mensajeFinal

let processingItem;

let loadDebugState = true;
function setupDebugState() {
    if (loadDebugState) { 
        loadDebugState = false
        duracionMostrarDibujo = 1000
        // timerEsperaMax = 1000

        // currentIndex = 59
        isTutorial = false
        drawPersonaje = false


        // utils.areaDibujoStartGrowAnimation()
        // pausaDescanso.index = 1

        if (!(isTutorial && currentIndex == 0)) {
            hideImage = false
        }
    }
}

export function initializeState(_formData, _listItems, _tutorialItems) {
    formData = _formData;
    if (DEBUG) {
        listItems = _listItems;
    } else {
        listItems = helpers.shuffleArray(_listItems)
        // let rr = ''
        // listItems.forEach(item => {
        //     rr += item.Row_number + ", "
        // });
        // console.log(rr)
    }

    tutorialItems = _tutorialItems
    isTutorial = true;
    // if (DEBUG) isTutorial = false
    registroGeneral = {
        nombre: formData.name,
        notas: formData.notes,
        lista: formData.list,
        timerEspera: formData.timerEspera,
        fecha: new Date().toLocaleDateString(),
        hora: new Date().toLocaleTimeString(),
        // interacciones: []
        items: []

    };
    timerEsperaMax = formData.timerEspera
    currentFondo = assets.fondosImgNames[0];
    currentPose = assets.poseImgNames[3]
    boca.current = assets.bocaImgNames[0]
    ojos.current = assets.ojosImgNames[0]
    currentIndex = 0
    expPausada = false
    timerCount = 0
    hideImage = true
    isAwaitingInteraction = false
    drawCortina = false
    showFeedback = false
    drawPersonaje = true
    senalar.setUsarAreaPersonaje2(false)
    processingItem = false
    utils.areaDibujoReset()

    contadorIncorrectas = 0
    motivacional = {
        show: false,
        index: 0
    }
    pausaDescanso = {
        show: false,
        index: 0,
        lastPauseIndex: -1,
        buttonIsPressed: false,
        showButton: false
    }
    mensajeFinal = {
        show: false,

    }

    ///

    if (DEBUG) {
        setupDebugState()

        helpers.setupHUD()

        setInterval(function () {
            helpers.updateHUD(currentIndex, isAwaitingInteraction,
                startTime, timerCount,
                hideImage, isTutorial)

        }, 100)
    }
    let btnPausa = dom.domget("btnPausa")

    btnPausa.addEventListener('click', () => pausarExperiencia())

    checkFondo()

    utils.requestWakeLock()
}

export async function pausarExperiencia(_pausar) {
    // expPausada = !expPausada
    expPausada = _pausar

    if (expPausada) {
        // console.log("timer Killed")
        dom.addClass('btnPausa', 'pressed')
        drawCortina = true
        isAwaitingInteraction = false
        boca.animar = false
        clearTimeout(attentionTimer);
        pauseAudio()
        boca.animar = false

    } else {
        dom.removeClass('btnPausa', 'pressed')

        isAwaitingInteraction = false
        clearTimeout(attentionTimer);

        // console.log("restart timer")
        drawCortina = false
        // console.log("pausaDescanso: ", pausaDescanso.show)
        if (pausaDescanso.show) {
            // console.log("hola")
            finishPausaDescanso()
        } else {

            await showItem(currentItem);
        }
    }
}
export async function startIntro() {
    // currentFondo = assets.fondosImgNames[0];
    checkFondo()
    currentPose = assets.poseImgNames[2]
    senalar.setUsarAreaPersonaje2(true)

    if (!DEBUG) {
        ojos.animar = true;
        await playAudio(assets.extras['musica_inicio'])
        ojos.animar = false;
        ojos.current = assets.ojosImgNames[0]
        await pausar(500)
        currentPose = assets.poseImgNames[3]
        await playSecuenciaAudio('Presentacion')
    }
    // boca.animar = true;
    // await playAudio(assets.extras['Presentacion_d.mp3'])
    // boca.animar = false;

    currentPose = assets.poseImgNames[1]
    senalar.setUsarAreaPersonaje2(false)

    await pausar(1000)
    // }

    if (DEBUG) {
        if (isTutorial && currentIndex == 0) {
        } else {

            utils.areaDibujoStartGrowAnimation()
        }
    }


    nextItem()
}

export async function nextItem(_iniciarPractica = false) {
    // console.log(currentIndex)
    // if(processingItem) return
    // processingItem=true
    if (expPausada) {
        drawCortina = true
        return
    }

    await checkMotivacional(currentIndex)

    if (await checkPausaDescanso(currentIndex)) {
        // console.log("EN DESCANSO")
        return
    }

    /// COMPORTAMIENTO ANTES DE MOSTRAR IMAGEN
    // if (DEBUG) console.log("/// inicio item")
    dom.domget("stateValue2").innerHTML = "Inicio Item"

    if (isTutorial) {
        if (currentIndex >= tutorialItems.length) {
            console.log('TUTORIAL FINALIZADO--------');
            isTutorial = false;
            currentIndex = 0;
            hideImage = true;
            senalar.setUsarAreaPersonaje2(true)
            drawPersonaje = true

            currentPose = assets.poseImgNames[6]
            await playSecuenciaAudio("Transicion")
            drawPersonaje = false
            senalar.setUsarAreaPersonaje2(false)

            await pausar(1000)

            nextItem()
            return;
        }
    }
    // else {
    //     /// FONDOS

    // }
    checkFondo(currentIndex)

    if (await showMensajeFinal(currentIndex)) {
        senalar.volverAlInicio()
        return
    }

    if (darExplicacion) {
        darExplicacion = false

        if (currentIndex == 0) {
            await pausar(1000);
            await playSecuenciaAudio("Instrucciones3")
            await pausar(100);

            titilar = DER
            currentPose = assets.poseImgNames[4]
            ojos.current = assets.ojosImgNames[1]
            await playSecuenciaAudio("Instrucciones4")
            titilar = NO

            currentPose = assets.poseImgNames[1]
            ojos.current = assets.ojosImgNames[0]
            await pausar(1000);
            await playSecuenciaAudio("Instrucciones5")
            await pausar(1000);
        }
        if (currentIndex == 1) {
            /// CEBRA
            await pausar(1000);
            await playSecuenciaAudio("Instrucciones7")
            await pausar(100);

            titilar = DER
            currentPose = assets.poseImgNames[4]
            ojos.current = assets.ojosImgNames[1]
            await playSecuenciaAudio("Instrucciones8")
            await pausar(1000);
            titilar = NO

            hideImage = true;
            currentPose = assets.poseImgNames[2]
            ojos.current = assets.ojosImgNames[0]
            await playSecuenciaAudio("Instrucciones9")
            await pausar(1000);

            darExplicacion = true

        }
        if (currentIndex == 2) {
            // if (!DEBUG) {
                await pausar(1000);
                await playSecuenciaAudio("Instrucciones11")
                await pausar(100);
            // }

            titilar = IZQ
            currentPose = assets.poseImgNames[5]
            ojos.current = assets.ojosImgNames[1]
            if (!DEBUG) {
                await playSecuenciaAudio("Instrucciones8")
                await pausar(1000)
            }
            titilar = NO

            _iniciarPractica = true


        }

        //////// FEEDBACK RESPUESTAS PRUEBA
        if (currentIndex >= 3) {
            /// respuesta negativa de la prueba
            // if (DEBUG) {
            //     let res = "INCORRECTA"
            //     if (feedback == POSITIVO) res = "CORRECTA"
            //     console.log("respuesta prueba: ", res)
            // }

            if (feedback == POSITIVO) {
                /// RESPUESTA CORRECTA
                // contadorIncorrectas = 0
            }
            if (feedback == NEUTRO) {
                /// RESPUESTA INCORRECTA
                contadorIncorrectas++
                console.log("Contador incorrectas: ", contadorIncorrectas)
                if (contadorIncorrectas == 1) {
                    drawCortina = true
                    await pausar(500);
                    await playSecuenciaAudio("Practica3")
                    await pausar(500);
                    restartTimer()
                    drawCortina = false

                    isAwaitingInteraction = true;
                    return
                }
                if (contadorIncorrectas == 2) {
                    titilar = IZQ
                    if (currentItem.ubic_correcta.trim() == "der") titilar = DER
                    await pausar(500);
                    await playSecuenciaAudio("Practica4")
                    await pausar(500);
                    titilar = NO
                    if (currentIndex < 5) {
                        /// si no es el ultimo, 
                        /// decir 'vamos a hacer otro'
                        await playSecuenciaAudio("Practica7_b")
                        await pausar(500);
                    }

                }
            }
        }
        currentIndex++;


    } else {
        if (isTutorial && currentIndex == 0) {
            // if (!DEBUG) {
            // await playSecuenciaAudio('Instrucciones1mono_')
            await playSecuenciaAudio('Instrucciones1mono_a')
            currentItem = tutorialItems[0];
            // console.log(currentItem)
            hideImage = false
            await playSecuenciaAudio('Instrucciones1mono_b')
            await playSecuenciaAudio('Instrucciones1mono_c')

            // }
        }
    }

    if (isTutorial) {
        if (currentIndex >= tutorialItems.length) {
            nextItem()
            return
        } else {
            currentItem = tutorialItems[currentIndex];

        }
    } else {
        currentItem = listItems[currentIndex];
    }
    contadorIncorrectas = 0
    // window.currentItem = currentItem



    if (darExplicacion) {
        darExplicacion = false
        if (currentIndex == 3) {
            _iniciarPractica = true

        }
        if (currentIndex == 2) {
            hideImage = false;
        }
    }
    if (!isTutorial && currentIndex == 0) hideImage = false

    if (_iniciarPractica) {
        senalar.setUsarAreaPersonaje2(true)
        hideImage = true;
        ojos.current = assets.ojosImgNames[0]
        currentPose = assets.poseImgNames[1]
        await pausar(1000);
        await playSecuenciaAudio("Practica1")

        hideImage = false;
        drawPersonaje = false
        await pausar(1000);
        utils.areaDibujoStartGrowAnimation()
        // await pausar(2500);
        await pausar(500);

    }
    // processingItem=false

    await showItem(currentItem);
    await pausar(1000)

}

async function showItem(item, _resetTimerCount = true, _notDrawCortina = false) {
    console.log("// showItem: ", currentIndex)
    // dom.domget("stateValue1").innerHTML = currentIndex
    // console.log(item)
    if (expPausada) {
        drawCortina = true
        return
    }

    if (pausaDescanso.lastPauseIndex == currentIndex) {
        hideImage = false
    }

    if (_resetTimerCount) {
        await pausar(duracionMostrarDibujo);
    } else {
        // console.log("no pausar para mostrar imagen")
    }

    if ((isTutorial && currentIndex == 0) || _notDrawCortina) {

    } else {
        drawCortina = true;
    }
    await pausar(500);


    ////// SAY ORACION ////////////////////////////////
    // boca.animar = true;
    await sayOracion(item.oracion);
    // boca.animar = false;
    await pausar(500);



    /// COMPORTAMIENTO DESPUES DE MOSTRAR IMAGEN
    // if (DEBUG) console.log("--- Despues de oracion")
    dom.domget("stateValue2").innerHTML = "Despues de oracion"


    if (isTutorial) {
        if (currentIndex == 0) {
            await pausar(500);
            await playSecuenciaAudio("Instrucciones2")
        }
        if (currentIndex == 1) {
            await pausar(500);
            await playSecuenciaAudio("Instrucciones6")
        }
        if (currentIndex == 2) {
            await pausar(500);
            await playSecuenciaAudio("Instrucciones10")
        }

        if (currentIndex == 3) {
            await pausar(500);
            await playSecuenciaAudio("Practica2")
        }

    }
    drawCortina = false;
    if (expPausada) {
        drawCortina = true
        return
    }

    isAwaitingInteraction = true;

    if (_resetTimerCount) timerCount = 0;
    // responseTime = 0;

    // Iniciar temporizador de atención
    // console.log("*** start timer resp")
    // attentionTimer = setTimeout(() => {
    //     console.log("***XX End timer");
    // }, 6000);
    await restartTimer()
}
async function restartTimer() {
    if (expPausada) return
    // responseTime = 0;
    // clearTimeout(attentionTimer);

    startTime = Date.now(); // Guardar el tiempo de inicio


    if (isTutorial && currentIndex < 3) {

    } else {
        attentionTimer = setTimeout(() => {
            handleAttentionTimeout(
                // isAwaitingInteraction, responseTime,
                // startTime, 
                // timerCount,
                // drawCortina,
                // pausar, playSecuenciaAudio,
                // showItem, boca,
                // playAudio,
                // restartTimer,
                // isTutorial,
                // titilar, currentItem, currentIndex
            )
        }, timerEsperaMax);
    }

}
async function handleAttentionTimeout() {
    // console.log("***XX End timer");
    isAwaitingInteraction = false;

    // responseTime = Date.now() - startTime; // Calcular el tiempo de respuesta
    timerCount++;

    if (timerCount === 1) {
        // console.log("PRIMER TIMEOUT")
        drawCortina = true
        await pausar(500)
        await playSecuenciaAudio("Practica5")
        // console.log("esperando interaccion neuvamente...")
        // isAwaitingInteraction = true;
        await showItem(currentItem, false)
    } else if (timerCount === 2) {
        // console.log("SEGUNDO TIMEOUT")
        drawCortina = true
        await pausar(500)
        boca.silbando = true
        await playAudio(assets.extras['silbido'])
        boca.silbando = false
        await playSecuenciaAudio("Practica6")
        // await showItem(currentItem, false, true)
        // await pausar(500)
        drawCortina = false

        await restartTimer();
        isAwaitingInteraction = true;

    } else if (timerCount >= 3) {
        // console.log("TERCER TIMEOUT")
        await pausar(500)
        if (isTutorial) {

            titilar = IZQ
            if (currentItem.ubic_correcta.trim() == "der") titilar = DER
            await pausar(500);
            if (currentIndex == 5) {
                /// ultimo item de practica
                await playSecuenciaAudio("Practica7_a")
                await pausar(500)
            } else {
                await playSecuenciaAudio("Practica7")
            }
            titilar = NO
            await pausar(500)
            registrarItem("-")

            currentIndex++;
            // console.log("ATENCION:", isTutorial, currentIndex, tutorialItems.length)
            if (currentIndex == 3) {
                await nextItem(true)
            } else {
                await nextItem()
            }
        } else {
            if (currentIndex < listItems.length - 1) {
                await pausar(500);
                await playSecuenciaAudio("Prueba_pasemos")
            }
            registrarItem("-")
            currentIndex++;
            await nextItem()

        }

        // // silvido
        // await playSecuenciaAudio("Practica7")
        // await showItem(currentItem, false, false)
    }

    // Reiniciar el temporizador si no hemos llegado al tercer timeout
    // if (timerCount < 2) {
    //     attentionTimer = setTimeout(handleAttentionTimeout, 6000);
    // }
}
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

export function handleMousePressed(p, areaDibujo) {

    if (!isAwaitingInteraction) return;

    // console.log("click")

    if (pausaDescanso.show) {
        // p.width * 0.75 - w * 0.5, p.height * 0.5 - w * 0.5, w, w
        let w = p.height * 0.33
        let x = p.width * 0.75 - w * 0.5
        let y = p.height * 0.5 - w * 0.5
        if (p.mouseX > x) {
            console.log("1")
            if (p.mouseX < (x + w)) {
                console.log("2")
                if (p.mouseY > y) {
                    console.log("3")
                    if (p.mouseY < y + w) {
                        console.log("4")
                        isAwaitingInteraction = false;
                        finishPausaDescanso()

                    }
                }
            }

        }
        return
    }
    isAwaitingInteraction = false;


    let section = utils.getSection(p.mouseX, p.mouseY, areaDibujo).trim();
    // console.log(section)
    // console.log(p.mouseX, p.mouseY)

    if (section === 'afuera') {
        isAwaitingInteraction = true;
        return;
    }


    clearTimeout(attentionTimer);
    let respuestaCorrecta = currentItem.ubic_correcta.trim();
    let acierto = 0;
    if (section === respuestaCorrecta) {
        feedback = POSITIVO
        acierto = 1
    } else {
        feedback = NEUTRO
    }

    let coords = utils.getCoordenadasNormalizadas(p.mouseX, p.mouseY, areaDibujo)
    // console.log(coords)
    registrarItem(section, acierto, coords)

    /// mnostrar feedback solo en tutorial!
    if (isTutorial) {
        showFeedback = true;
        drawCortina = true;

        p.redraw();  // Force redraw to show message and cortina
        setTimeout(() => {
            showFeedback = false;
            drawCortina = false;
            // if (isTutorial && currentIndex == 0 ||
            //     isTutorial && currentIndex == 1 ||
            //     isTutorial && currentIndex == 2 ||
            //     isTutorial && currentIndex == 3
            // ) {
            // if (isTutorial) {
            darExplicacion = true
            // } else {
            // currentIndex++;
            // }
            nextItem();
        }, 2000);
    } else {
        currentIndex++;
        nextItem();
    }
}
function registrarItem(section, acierto = 0, coords = "-") {
    let elapsed = Date.now() - startTime;
    if (section == "-") {
        elapsed = "-"
    }
    let x = '-'
    let y = '-'
    if (coords != '-') {
        x = coords.x
        y = coords.y

    }

    let orden = 0;
    if (registroGeneral.items.length > 0) orden = registroGeneral.items.length
    registroGeneral.items.push({
        // item: currentItem,
        ...currentItem,
        respuesta: section,
        tiempoDeRespuesta: elapsed,
        recordatorios: timerCount,
        Acierto: acierto,
        coordX: x,
        coordY: y,
        orden: orden,
        tiempo: timerEsperaMax,
    });
}
export function updateAnimations() {
    helpers.updateAnimations2(boca, ojos)
}

async function checkMotivacional(index) {
    if (!isTutorial) {
        if (
            index == 6
            || index == 13
            || index == 28
            || index == 34
            || index == 50

        ) {
            // motivacional.index++
            // motivacional.index = 5
            if (index == 6) motivacional.index = 1
            if (index == 13) motivacional.index = 2
            if (index == 28) motivacional.index = 3
            if (index == 34) motivacional.index = 4
            if (index == 50) motivacional.index = 5

            dom.domget("stateValue2").innerHTML = "MOTIVACIONAL"
            drawCortina = true
            hideImage = true
            motivacional.show = true

            await playAudio(assets.extras['musica_motivacional'])
            let filename = 'Msj_motivacional_' + (motivacional.index);
            if (motivacional.index == 5) filename = 'Msj_motivacional_3'
            console.log(filename)
            await playSecuenciaAudio(filename)
            await pausar(1000)

            motivacional.show = false
            hideImage = false
            drawCortina = false
        }
    }
}


function checkFondo(index) {
    if (isTutorial) {
        currentFondo = assets.fondosImgNames[4];
    } else {
        if (index < 20) {
            currentFondo = assets.fondosImgNames[9];
        } else if (index >= 20 && index < 40) {
            currentFondo = assets.fondosImgNames[3];
        } else if (index >= 40) {
            currentFondo = assets.fondosImgNames[1];
        }
    }
}

async function checkPausaDescanso(index) {
    let isDescanso = false
    if (index == 20 || index == 40) {
        if (index != pausaDescanso.lastPauseIndex) {
            pausaDescanso.index++

            dom.domget("stateValue2").innerHTML = "DESCANSO"
            currentPose = assets.poseImgNames[2]

            drawCortina = false
            hideImage = true
            drawPersonaje = true
            senalar.setUsarAreaPersonaje2(true)

            pausaDescanso.show = true

            let secuencia = 'Pausa_1';
            currentFondo = assets.fondosImgNames[8];
            if (pausaDescanso.index > 1) {
                secuencia = 'Pausa_2'
                currentFondo = assets.fondosImgNames[2];
            }

            await playSecuenciaAudio(secuencia)
            await pausar(500)

            pausaDescanso.lastPauseIndex = index
            isDescanso = true

            pausaDescanso.showButton = true
            isAwaitingInteraction = true
            // pausaDescanso.show = false
            // drawPersonaje = false
            // // hideImage = false
        } else {
            await pausar(1000)
        }
    }
    return isDescanso
}

function finishPausaDescanso() {
    pausaDescanso.buttonIsPressed = true
    setTimeout(function () {
        pausaDescanso.buttonIsPressed = false
        pausaDescanso.show = false
        pausaDescanso.showButton = false
        drawPersonaje = false
        senalar.setUsarAreaPersonaje2(false)

        checkFondo(currentIndex + 1)
        // hideImage = false
        nextItem()
    }, 500)
}

async function showMensajeFinal(index) {
    if(isTutorial) return
    
    let respuesta = false
    if (index >= listItems.length) {
        console.log('Test finalizado');
        drawCortina = true
        dom.addClass('miniLoader', 'show')

        // console.log(registroGeneral);
        if (!DEBUG) {
            let todoOk = await fb.pushNewRegistro(registroGeneral)
            console.log("Guardando registro: ", todoOk)
            if (todoOk != "ok") {
                alert(todoOk)
            }
        } else {
            // await pausar(1000)
            let todoOk = await fb.pushNewRegistro(registroGeneral)
            console.log("Guardando registro: ", todoOk)
            if (todoOk != "ok") {
                alert(todoOk)
            }
        }
        hideImage = true
        currentFondo = assets.fondosImgNames[6];
        drawCortina = false
        dom.removeClass('miniLoader', 'show')


        // await pausar(500)
        currentPose = assets.poseImgNames[0]
        drawPersonaje = true
        senalar.setUsarAreaPersonaje2(true)
        mensajeFinal.show = true

        // if (DEBUG) {
        //     await playSecuenciaAudio("Msj_final_c")
        //     await pausar(500)
        // } else {
        await playAudio(assets.extras['musica_final'])
        await pausar(1000)
        await playSecuenciaAudio("Msj_final")
        await pausar(500)
        // }


        respuesta = true
    }
    return respuesta;
}

