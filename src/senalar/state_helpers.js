import * as assets from './assets.js';
import * as utils from './utils.js';
import { POSITIVO, NEUTRO } from './state.js';
import * as state from './state.js';

import * as dom from '../dom-helpers.js'


// export function handleMousePressed2(p, areaDibujo,
//     isAwaitingInteraction,
//     attentionTimer,
//     startTime,
//     registroGeneral,
//     darExplicacion,
//     currentItem,
//     feedback,
//     showFeedback,
//     drawCortina,
//     nextItem,
//     isTutorial
// ) {
//     if (!isAwaitingInteraction) return;
//     isAwaitingInteraction = false;

//     clearTimeout(attentionTimer);

//     let section = utils.getSection(p.mouseX, p.mouseY, areaDibujo).trim();

//     if (section === 'afuera') {
//         isAwaitingInteraction = true;
//         return;
//     }

//     let elapsed = Date.now() - startTime;
//     let respuestaCorrecta = currentItem.Respuesta_correcta.trim();

//     if (section === respuestaCorrecta) {
//         // messageText = "Correcto";
//         // messageColor = p.color(0, 255, 0);
//         feedback = POSITIVO
//     } else {
//         // messageText = "Incorrecto";
//         // messageColor = p.color(255, 0, 0);
//         feedback = NEUTRO
//     }

//     showFeedback = true;
//     drawCortina = true;

//     registroGeneral.interacciones.push({
//         item: currentItem,
//         respuesta: section,
//         tiempo: elapsed
//     });

//     p.redraw();  // Force redraw to show message and cortina


//     setTimeout(() => {
//         showFeedback = false;
//         drawCortina = false;
//         // if (isTutorial && currentIndex == 0 ||
//         //     isTutorial && currentIndex == 1 ||
//         //     isTutorial && currentIndex == 2 ||
//         //     isTutorial && currentIndex == 3
//         // ) {
//         if (isTutorial) {
//             darExplicacion = true
//         } else {
//             currentIndex++;
//         }
//         nextItem();
//     }, 2000);
// }

export function updateAnimations2(boca, ojos) {
    if (boca.animar) {
        boca.counter++
        if (boca.counter % boca.maxCounter == 0) {
            boca.animationIndex = (boca.animationIndex + 1) % assets.bocaImgNames.length
            boca.current = assets.bocaImgNames[boca.animationIndex]
        }

    } else {
        if (boca.silbando) {
            boca.current = assets.bocaImgNames[3]

        } else {
            boca.current = assets.bocaImgNames[1]

        }
    }

    if (ojos.animar) {
        ojos.counter++
        if (ojos.counter % ojos.maxCounter == 0) {
            ojos.animationIndex = (ojos.animationIndex + 1) % assets.ojosImgNames.length
            ojos.current = assets.ojosImgNames[ojos.animationIndex]
        }

    } else {
        // ojos.current = assets.ojosImgNames[0]
    }
}

export function setupHUD() {
    dom.domget("stateName1").innerHTML = "currentIndex"
    dom.domget("stateName2").innerHTML = "momento"
    dom.domget("stateName3").innerHTML = "timer"
    dom.domget("stateName4").innerHTML = "timerCount"
    dom.domget("stateName5").innerHTML = "audio"
    dom.domget("stateName6").innerHTML = "hideImage"
    dom.domget("stateName7").innerHTML = "image"
    dom.domget("stateName8").innerHTML = "istutorial"
    dom.domget("stateName9").innerHTML = "motivacional.index"
    dom.domget("stateName10").innerHTML = "currentFondo"
    dom.domget("stateName11").innerHTML = "pausaDescanso.show"
}
export function updateHUD(
    currentIndex, isAwaitingInteraction,
    startTime, timerCount,
    hideImage, isTutorial
) {
    dom.domget("stateValue1").innerHTML = currentIndex

    if (isAwaitingInteraction) {
        dom.domget("stateValue3").innerHTML = Date.now() - startTime
    }
    dom.domget("stateValue4").innerHTML = timerCount
    dom.domget("stateValue6").innerHTML = hideImage
    dom.domget("stateValue8").innerHTML = isTutorial
    dom.domget("stateValue9").innerHTML = state.motivacional.index
    dom.domget("stateValue10").innerHTML = state.currentFondo
    dom.domget("stateValue11").innerHTML = state.pausaDescanso.show



}

export function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}