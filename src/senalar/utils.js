
import * as dom from '../dom-helpers'

//////////////////
export function pausar(milisegundos) {
    return new Promise((resolve) => {
        setTimeout(resolve, milisegundos);
    });
}

export function getSection(x, y, area) {
    if (y > area.y && y < area.y + area.h) {
        if (x > area.x && x < area.inflexion1) return 'izq'
        if (x > area.inflexion2 && x < area.x + area.w) return 'der'
        if (x > area.inflexion1 && x < area.inflexion2) return 'centro'
    }
    return "afuera"
}
export function getCoordenadasNormalizadas(x, y, area) {
    if (y > area.y && y < area.y + area.h) {
        if (x > area.x && x < area.x + area.w) {
            let cx = ((x - area.x) / area.w).toFixed(2);
            let cy = ((y - area.y) / area.h).toFixed(2);
            // console.log(cx, cy);
            return { x: cx, y: cy }; // Retorna los valores si es necesario

            // return (cx + ", " + cy)
        }
    }
    // return null;
}
///////////////
let dibujoChico;
let dibujoGrande = {};
export let animarArea = false;
let areaLerpAmt;
let areaLerpStep = 0.05;
export function setupAreasDraw(p, dibujo, personaje, personaje2) {
    // dibujo.w = p.width * 0.55;
    // dibujo.h = dibujo.w * 0.555;
    dibujo.h = p.height * 0.55;
    dibujo.w = dibujo.h * 1.8;

    dibujo.pctAnchoAreaClick = 0.37
    dibujo.anchoAreaClick = dibujo.w * dibujo.pctAnchoAreaClick

    dibujo.x = p.width / 2 - dibujo.w / 2;
    // dibujo.y = p.height / 2 - dibujo.h / 2;
    dibujo.y = dibujo.h * 0.1

    // dibujo.inflexion1 = dibujo.x + dibujo.w * 0.333;
    // dibujo.inflexion2 = dibujo.x + dibujo.w * 0.666;
    dibujo.inflexion1 = dibujo.x + dibujo.anchoAreaClick;
    dibujo.inflexion2 = dibujo.x + dibujo.w - dibujo.anchoAreaClick;

    dibujoChico = JSON.parse(JSON.stringify(dibujo))
    dibujoGrande.w = dibujoChico.w * 1.25
    dibujoGrande.h = dibujoChico.h * 1.25
    dibujoGrande.x = p.width * 0.5 - dibujoGrande.w * 0.5
    dibujoGrande.y = p.height * 0.5 - dibujoGrande.h * 0.5

    personaje.w = dibujo.w * 0.43
    personaje.h = personaje.w * 1.89
    personaje.x = (p.width * 0.5) - personaje.w * 0.5
    // personaje.y = (dibujo.y + dibujo.h) + dibujo.h * 0.1
    personaje.y = p.height - 0.6 * personaje.h

    personaje2.w = dibujo.w * 0.4
    personaje2.h = personaje2.w * 1.89
    personaje2.x = (p.width * 0.5) - personaje2.w * 0.5
    // personaje.y = (dibujo.y + dibujo.h) + dibujo.h * 0.1
    // personaje2.y = p.height - 0.6 * personaje2.h
    personaje2.y = p.height * 0.5 - 0.28 * personaje2.h


    areaDibujoReset()
}
export function areaDibujoStartGrowAnimation() {
    animarArea = true;
    // console.log("area dibujo start grow")

}
export function areaDibujoReset() {
    // console.log("area dibujo reset")
    areaLerpAmt = 0
    animarArea = false;

}
export function updateAreaDibujoPos(p, dibujo) {
    if (animarArea) {
        areaLerpAmt += areaLerpStep
        if (areaLerpAmt > 1) areaLerpAmt = 1
        dibujo.x = p.lerp(dibujoChico.x, dibujoGrande.x, areaLerpAmt)
        dibujo.y = p.lerp(dibujoChico.y, dibujoGrande.y, areaLerpAmt)
        dibujo.w = p.lerp(dibujoChico.w, dibujoGrande.w, areaLerpAmt)
        dibujo.h = p.lerp(dibujoChico.h, dibujoGrande.h, areaLerpAmt)
        // dibujo.inflexion1 = dibujo.x + dibujo.w * 0.333;
        // dibujo.inflexion2 = dibujo.x + dibujo.w * 0.666;
        dibujo.anchoAreaClick = dibujo.w * dibujo.pctAnchoAreaClick

        dibujo.inflexion1 = dibujo.x + dibujo.anchoAreaClick;
        dibujo.inflexion2 = dibujo.x + dibujo.w - dibujo.anchoAreaClick;
    }
}


export function activarPantallaCompleta(activate = true) {
    let elemento = document.documentElement;  // Obtiene el elemento <html>

    if (activate) {
        if (elemento.requestFullscreen) {
            elemento.requestFullscreen();
        } else if (elemento.mozRequestFullScreen) {  /* Firefox */
            elemento.mozRequestFullScreen();
        } else if (elemento.webkitRequestFullscreen) {  /* Chrome, Safari & Opera */
            elemento.webkitRequestFullscreen();
        } else if (elemento.msRequestFullscreen) {  /* IE/Edge */
            elemento.msRequestFullscreen();
        }
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {  /* Firefox */
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {  /* Chrome, Safari & Opera */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {  /* IE/Edge */
            document.msExitFullscreen();
        }
    }
}
export function isInFullScreen() {
    return !!(document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement);
}

let wakeLock = null;

export const requestWakeLock = async () => {
    try {
        wakeLock = await navigator.wakeLock.request('screen');
        wakeLock.addEventListener('release', () => {
            // console.log('Wake Lock fue liberado');
        });
        // console.log('Wake Lock está activo');
    } catch (err) {
        console.error(`No se pudo obtener el wake lock: ${err.name}, ${err.message}`);
    }
}


export function showPassword(visible) {
    if (visible) {
        dom.get("pass").value = ''
        dom.addClass("containerPassword", "show")
    } else {
        dom.removeClass("containerPassword", "show")
    }
}