import * as items from './items.js';
import * as utils from './utils.js';
// import { preloadAssets, assets, fondoImages, personajePoses, personajeBocas  } from './assets.js';
import * as assets from './assets.js';
// import { initializeState, nextItem, handleMousePressed, areaDibujo, currentFondo, currentItem, showMessage, messageText, messageColor, drawCortina, currentPose, currentBoca } from './state.js';
import * as state from './state.js';
import { domget } from '../dom-helpers.js';
import * as dom from '../dom-helpers.js';


export const DEBUG = false; 

let formData = {};
let listItems = [];
let tutorialItems = [];
export let areaDibujo = { x: 0, y: 0, w: 0, h: 0, inflexion1: 0, inflexion2: 0 };
export let areaPersonaje = { x: 0, y: 0, w: 0, h: 0 };
export let areaPersonaje2 = { x: 0, y: 0, w: 0, h: 0 };
export let usarAreaPersonaje2
export let setUsarAreaPersonaje2 = function (value) { usarAreaPersonaje2 = value }

let titilando = false
let titilarFramerate = 8

let drawLimits = false
let myP5Instance

export function start(_formData) {
    formData = _formData;
    listItems = items.listas[formData.list];
    tutorialItems = items.listas["tutorial"];
    // console.log(listItems)
    // console.log(tutorialItems)
    // tutorialItems.forEach((item, i) => {
    //     console.log(i, item.oracion)
    // });



    state.initializeState(formData, listItems, tutorialItems);
    window.state = state

    if (myP5Instance) myP5Instance.remove();
    myP5Instance = new p5(sketch);
    // new p5(sketch);

    if (DEBUG) {
        domget("containerHud").style.display = 'flex'
    }



}

function sketch(p) {
    p.preload = () => {
        assets.preloadAssets(p, listItems, tutorialItems);
    };

    p.setup = () => {
        const canvasContainer = document.querySelector("#containerCanvas");
        const canvas = p.createCanvas(window.innerWidth, window.innerHeight);
        p.frameRate(30);
        canvas.parent(canvasContainer);
        utils.setupAreasDraw(p, areaDibujo, areaPersonaje, areaPersonaje2);
        // state.nextItem();
        state.startIntro();

    };
    p.windowResized = () => {
        p.resizeCanvas(window.innerWidth, window.innerHeight);
        utils.setupAreasDraw(p, areaDibujo, areaPersonaje, areaPersonaje2);
    };

    p.mousePressed = () => {
        state.handleMousePressed(p, areaDibujo);
    };
    p.mouseMoved = () => {
        // let section = utils.getSection(p.mouseX, p.mouseY, areaDibujo).trim();
        // console.log(section)
        // utils.getCoordenadasNormalizadas(p.mouseX, p.mouseY, areaDibujo)
    }
    p.draw = () => {
        p.background("#333333");
        p.image(assets.assets[state.currentFondo], 0, 0, p.width, p.height);

        // p.fill(255);
        // p.rect(areaDibujo.x, areaDibujo.y, areaDibujo.w, areaDibujo.h);


        // if (state.currentItem && !state.showFeedback && !state.hideImage) {
        if (state.currentItem && !state.hideImage) {

            const img = assets.assets[`${state.currentItem.Nombre_dibujo}.png`];
            domget("stateValue7").innerHTML = `${state.currentItem.Nombre_dibujo}.png`

            if (img) {
                /// IMAGEN:
                // let radius = 20;
                // let mask = p.createGraphics(img.width, img.height);
                // mask.background(0);
                // mask.noStroke();
                // mask.fill(255);
                // mask.rect(0, 0, img.width*0.5, img.height, radius);
                // img.mask(mask);
                p.image(img, areaDibujo.x, areaDibujo.y, areaDibujo.w, areaDibujo.h);


                /// BORDE NEGRO:
                p.push();
                p.noFill();
                p.stroke(0);
                p.strokeWeight(3);
                p.rect(areaDibujo.x, areaDibujo.y, areaDibujo.w, areaDibujo.h);

                if (drawLimits) {
                    p.strokeWeight(1);
                    p.stroke(0, 30);
                    // p.stroke(255, 0, 0);
                    // p.rect(areaDibujo.x, areaDibujo.y, areaDibujo.w * 0.333, areaDibujo.h);
                    p.rect(areaDibujo.x, areaDibujo.y, areaDibujo.anchoAreaClick, areaDibujo.h);

                    // p.stroke(0, 255, 0);
                    p.rect(areaDibujo.inflexion1, areaDibujo.y, areaDibujo.w - areaDibujo.anchoAreaClick * 2, areaDibujo.h);

                    // p.stroke(0, 0, 255);
                    p.rect(areaDibujo.inflexion2, areaDibujo.y, areaDibujo.anchoAreaClick, areaDibujo.h);
                }

                /// rectangulos titilando
                if (state.titilar != state.NO) {
                    if (p.frameCount % titilarFramerate == 0) {
                        titilando = !titilando
                    }
                    if (titilando) {
                        p.stroke(255, 0, 180);
                        p.strokeWeight(5);
                        if (state.titilar == state.IZQ) {
                            p.rect(areaDibujo.x, areaDibujo.y, areaDibujo.anchoAreaClick, areaDibujo.h);
                        } else if (state.titilar == state.DER) {
                            p.rect(areaDibujo.inflexion2, areaDibujo.y, areaDibujo.anchoAreaClick, areaDibujo.h);
                        }
                    }
                }

                p.pop();
            }
        }

        if (state.drawPersonaje && !state.showFeedback) {
            let area;
            if (usarAreaPersonaje2) {
                area = areaPersonaje2
            } else {
                area = areaPersonaje
            }
            p.image(assets.assets[state.currentPose], area.x, area.y, area.w, area.h)
            if (state.ojos.animar) {
                // console.log(assets.assets[state.ojos.current])
                p.image(assets.assets[state.ojos.current], area.x, area.y, area.w, area.h)
            } else {
                p.image(assets.assets[state.boca.current], area.x, area.y, area.w, area.h)
            }
        }

        if (state.drawCortina) {
            p.fill(0, 0.6 * 255);
            p.rect(0, 0, p.width, p.height);
        }

        if (state.showFeedback) {
            // p.fill(state.messageColor);
            // p.textSize(32);
            // p.textAlign(p.CENTER, p.CENTER);
            // p.text(state.messageText, p.width / 2, p.height / 2);
            // p.text(state.feedback, p.width / 2, p.height / 2);
            let img = assets.extras['emoji_positivo']
            if (state.feedback == state.NEUTRO)
                img = assets.extras['emoji_neutro']
            let w = p.height * 0.35
            p.image(img, p.width * 0.5 - w * 0.5, p.height * 0.5 - w * 0.5, w, w)

        }

        if (state.motivacional.show) {
            let img = assets.extras['estrella']
            // let w = p.height * 0.25
            // p.image(img, p.width * 0.5 - w * 0.5, p.height * 0.5 - w * 0.5, w, w)
            let aspectRatio = img.width / img.height;
            let newWidth = p.height * aspectRatio;
            p.image(img, (p.width - newWidth) / 2, 0, newWidth, p.height);
        }

        if (state.pausaDescanso.show) {
            let img;
            let w, h;
            if (state.pausaDescanso.showButton) {
                if (state.pausaDescanso.buttonIsPressed) {
                    img = assets.extras['boton_play2']
                } else {
                    img = assets.extras['boton_play1']
                }
                w = p.height * 0.33
                p.image(img, p.width * 0.75 - w * 0.5, p.height * 0.5 - w * 0.5, w, w)
            }

            let imgEstrellas
            if (state.pausaDescanso.index == 1) {
                imgEstrellas = assets.extras['contador_estrellas_1']
            } else {
                imgEstrellas = assets.extras['contador_estrellas_2']
            }
            h = p.height * 0.3
            w = h * 2
            p.image(imgEstrellas, p.width * 0.5 - w * 0.5, p.height * 0.2 - h * 0.5, w, h)
        }

        if (state.mensajeFinal.show) {
            let imgConfeti = assets.extras['confeti']
            p.image(imgConfeti, 0, 0, p.width, p.height)

            let imgEstrellas = assets.extras['contador_estrellas_3']
            let h = p.height * 0.3
            let w = h * 2
            p.image(imgEstrellas, p.width * 0.5 - w * 0.5, p.height * 0.2 - h * 0.5, w, h)

        }

        state.updateAnimations()

        utils.updateAreaDibujoPos(p, areaDibujo)
    };


}


export function volverAlInicio() {
    if (myP5Instance) myP5Instance.remove();

    // dom.addClass("containerFormulario", "show")
    dom.removeClass("containerCanvas", "show")
    dom.removeClass("containerSenalar", "show")
    dom.addClass("containerPantallaDeInicio", "show")
    

}