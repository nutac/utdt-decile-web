// export function processAudioFileName(oracion) {
//     return oracion
//         .replace(/[¿?]/g, '')
//         .replace(/ /g, '_')
//         .normalize('NFD').replace(/[\u0300-\u036f]/g, '')
//         .concat('.mp3');
// }
import { path_oraciones, processAudioFileName } from "./assets";
import * as assets from './assets.js';
import { pausar } from './utils.js';
import * as dom from '../dom-helpers.js'
import { DEBUG } from './senalar.js';



// import { DEBUG } from "./senalar";
let audioDebug = false
export let audioElement
export function sayOracion(oracion) {
    return new Promise((resolve, reject) => {
        let audioFile = processAudioFileName(oracion);
        let audioPath = path_oraciones + audioFile;
        audioElement = new Audio(audioPath);
        audioElement.currentTime = 0; // Reiniciar el audio al finalizar

        if (audioDebug) console.log("--playing: " + oracion)
        audioElement.onended = () => {
            if (audioDebug) console.log("----audio ended.");
            resolve();
        };

        audioElement.onerror = (err) => {
            console.error(`Error al reproducir el audio: ${audioFile}`, err);
            reject(err);
        };

        // console.log(`Reproduciendo audio: ${audioFile}`);
        audioElement.play().catch(err => {
            console.error(`Error al intentar reproducir el audio: ${audioFile}`, err);
            reject(err);
        });
    });
}
export function pauseAudio() {
    if (audioElement) {
        audioElement.pause(); // Pausa el audio
        audioElement.currentTime = 0; // Opcional: reinicia el audio al principio
    }
}
export async function playAudio2(audio) {
    ///  PROBAR IMPLEMENTAR ESTA FUNCION PARA 
    ///  PODER PRE-CARGAR LOS AUDIOS !!!
    return new Promise(resolve => {
        audio.onended = resolve;
        audio.play();
    });
}
export function playAudio(file) {
    return new Promise((resolve, reject) => {
        // let audio = new Audio(file);
        // let audio = new Audio(file);
        audioElement = new Audio(file);

        // console.log("--playing: " + file)
        audioElement.onended = () => {
            // console.log("----audio ended.");
            resolve();
        };

        audioElement.onerror = (err) => {
            console.error(`Error al reproducir el audio: ${file}`, err);
            reject(err);
        };

        audioElement.play().catch(err => {
            console.error(`Error al intentar reproducir el audio: ${file}`, err);
            reject(err);
        });
    });
}


export async function playSecuenciaAudio(nombreSecuencia) {
    // if (DEBUG) console.log(nombreSecuencia)
    let state = window.state
    // console.log(state)
    if (DEBUG)
        dom.domget("stateValue5").innerHTML = nombreSecuencia


    const audiosDeSecuencia = assets.audiosPersonajeNames.filter(name => name.startsWith(nombreSecuencia));
    if (audiosDeSecuencia.length > 0) {
        for (const audioName of audiosDeSecuencia) {
            state.boca.animar = true;
            await playAudio(assets.extras[audioName]);
            state.boca.animar = false;
            if (state.expPausada) {
                break
            }
            await pausar(500);
        }
    } else {
        console.log("No existe la secuencia o archivo: " + nombreSecuencia)
    }
}
