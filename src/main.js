import * as dom from './dom-helpers.js'
import * as items from './senalar/items.js'
import * as senalar from './senalar/senalar.js'
import * as fb from './firebase.js'
import * as state from './senalar/state.js'
import * as modalRegistros from './modalRegistros.js'
import * as utils from './senalar/utils.js'

document.getElementById('pass').addEventListener('input', function () {
    let val = this.value.toLowerCase()
    let word = 'torcuato'
    if (modalRegistros.showModal) word = 'ditella'
    if (val == word) {
        utils.showPassword(false);
    }
});




document.getElementById("btnEmparejamiento").addEventListener("click", () => {
    window.location.href = '/emparejamiento/index.html'
})

document.getElementById("btnSenalar").addEventListener("click", () => {
    dom.removeClass("containerPantallaDeInicio", "show")
    dom.addClass("containerSenalar", "show")
    dom.addClass("containerFormulario", "show")

    if (senalar.DEBUG) {
        document.querySelector('#name').value = "test_1"
    }

    items.loadList().then(() => {
        // console.log(items.listas)
        setupComboList(items.listas)

        if (senalar.DEBUG) {
            // startSenalarExperience()
        }
    })


})


//////////////  FORMULARIO SENALAR   ////////////////////////
let form = document.querySelector('#formulario');
if (senalar.DEBUG) {
    utils.showPassword(false)
    // document.getElementById("btnSenalar").click()
}
function setupComboList(lists) {
    let select = form.querySelector('#comboListas');
    Object.keys(lists).forEach((listName, index) => {
        let option = document.createElement('option');
        option.value = listName;
        option.text = listName;
        if (index === 1) {
            option.selected = true;
        }
        select.appendChild(option);
    });

}

/// BTN EMPEZAR/COMENZAR ///////////////////////////////
document.getElementById("btnSubmit")
    .addEventListener('click', () => {
        startSenalarExperience()
    })

function startSenalarExperience() {
    let formData = {
        name: form.querySelector('#name').value,
        notes: form.querySelector('#notes').value,
        list: form.querySelector('#comboListas').value,
        // timerEspera: form.querySelector('#timerEspera').value
        timerEspera: document.getElementById('timerEspera').value

    };
    if (formData.name != '' &&
        formData.list != ''
    ) {
        if (!senalar.DEBUG)
            utils.activarPantallaCompleta(true);

        senalar.start(formData)
        dom.removeClass("containerFormulario", "show")
        dom.addClass("containerCanvas", "show")

    }
}
///////////////
///////// MENU         ////////////////////////////////////////////////
let timeoutHiddenButton;
dom.get(btnMostrarMenuActividad).addEventListener('contextmenu', (e) => {
    e.preventDefault();
});
dom.get(btnMostrarMenuActividad).addEventListener('mousedown', (e) => {
    if (senalar.DEBUG) {
        pausarActividad(true);
    } else {
        timeoutHiddenButton = setTimeout(() => {
            pausarActividad(true);
            // console.log("mostrar menu")
        }, 2000);
    }
})
dom.get(btnMostrarMenuActividad).addEventListener('touchstart', (e) => {
    timeoutHiddenButton = setTimeout(() => {
        pausarActividad(true);
        // console.log("mostrar menu touch")
    }, 2000);
})

dom.get(btnMostrarMenuActividad).addEventListener('mouseup', (e) => {
    clearTimeout(timeoutHiddenButton);
})
dom.get(btnMostrarMenuActividad).addEventListener('touchend', (e) => {
    clearTimeout(timeoutHiddenButton);
})
function pausarActividad(pausar) {
    console.log("pausarr")
    if (dom.containClass("containerCanvas", "show")) {
        if (pausar) {
            dom.addClass("menuActividad", "show")
        } else {
            dom.removeClass("menuActividad", "show")
        }
        state.pausarExperiencia(pausar)

    }

}

dom.get("btnGuardarYVolverAlInicio").addEventListener('click', async () => {
    let todoOk = await fb.pushNewRegistro(state.registroGeneral)
    console.log("Guardando registro: ", todoOk)

    dom.removeClass("menuActividad", "show")
    senalar.volverAlInicio()
})
dom.get("btnVolverAlInicio").addEventListener('click', () => {
    if (!senalar.DEBUG) {
        dom.removeClass("menuActividad", "show")
        senalar.volverAlInicio()
    }
    if (senalar.DEBUG) {
        console.log(state.registroGeneral.items)
    }
})

dom.get("btnFullscreen").addEventListener('click', () => {
    if (utils.isInFullScreen()) {
        utils.activarPantallaCompleta(false);
    } else {
        utils.activarPantallaCompleta(true);

    }
})
dom.get("btnContinuarDesdePausa").addEventListener('click', () => {
    pausarActividad(false)
})


///////////////
/////////////////////////////////////////////////////////

// document.getElementById("btnRegistros").addEventListener('click', () => {
//     // console.log("cargando registros...")

//     modalRegistros.show()


//     // fb.getRegistros().then((registros) => {
//     //     console.log(registros);
//     //     // Aquí puedes manipular los datos como necesites
//     // }).catch((error) => {
//     //     console.error("Error fetching registros: ", error);
//     // });
// })

document.getElementById("btnTest").addEventListener('click', async () => {
    console.log("Grabando registro...")
    console.log(state.registroGeneral)
    let todoOk = await fb.pushNewRegistro(state.registroGeneral)
    console.log(todoOk)
    // saveTest().then((respuesta) => {
    //     console.log(respuesta);
    //     // Aquí puedes manipular los datos como necesites
    // }).catch((error) => {
    //     console.error("Error fetching registros: ", error);
    // });
})


document.addEventListener("visibilitychange", function () {
    if (document.hidden) {
        // console.log("La pantalla podría estar bloqueada o la app en segundo plano");
        pausarActividad(true);

    } else {
        // console.log("La pantalla está activa y la app en primer plano");
        // pausarActividad(false)

    }
});

function clickOpcion(index) {
    document.querySelectorAll('.opcionMenu').forEach((element, i) => {
        if (i === index) {
            element.classList.add('selected');
        } else {
            element.classList.remove('selected');
        }
    });
    
    let modalOpciones=document.getElementById('containerModalOpciones')
    if (index == 0) {
        modalRegistros.show(false)
        modalOpciones.classList.remove('show')


    } else if (index == 1) {
        modalRegistros.show(false)
        modalOpciones.classList.add('show')

    } else if (index == 2) {
        /// btnRegistros
        modalRegistros.show(true)
        modalOpciones.classList.remove('show')


    }
}

document.querySelectorAll('.opcionMenu').forEach((element, index) => {
    element.addEventListener('click', () => clickOpcion(index));
});

// function loadDataFile() {
//     fetch('/data/data/data.txt')
//         .then(response => {
//             if (!response.ok) {
//                 throw new Error('Network response was not ok');
//             }
//             return response.text();
//         })
//         .then(data => {
//             console.log(data); // Imprimir el contenido del archivo en la consola
//         })
//         .catch(error => {
//             console.error('Error fetching the data file:', error);
//         });
// }

// // Llamar a la función para cargar el archivo al iniciar
// loadDataFile();

