import * as dom from './dom-helpers.js'
import * as fb from './firebase.js'
import * as utils from './senalar/utils.js'
import * as senalar from './senalar/senalar.js'

export let showModal = false
let arrayRegistros = []

export function show(_show) {
    // showModal = !showModal
    showModal = _show

    if (!senalar.DEBUG)
        utils.showPassword(showModal)
    if (showModal) {

        dom.addClass('containerModalRegistro', 'show')
        dom.addClass('btnRegistros', 'pressed')

        requestLista()
        document.getElementById('filtro').addEventListener('input', filterRegistros);
        document.getElementById('filtroListas').addEventListener('change', filterRegistros);
    } else {
        dom.removeClass('containerModalRegistro', 'show')
        dom.removeClass('btnRegistros', 'pressed')


    }
}

//////////////////////////////////////////////////////////
function requestLista() {
    fb.getRegistros().then((elementos) => {
        arrayRegistros = [];
        const listasUnicas = new Set(); // Usamos un Set para evitar duplicados

        for (const key in elementos) {
            if (elementos.hasOwnProperty(key)) {
                let registro = elementos[key];
                let fila = {};
                for (const campo in registro) {
                    fila[campo] = registro[campo];
                }
                arrayRegistros.push(fila);
                listasUnicas.add(registro.lista); // Agregar la lista al Set
            }
        }

        // Poblar el select con las listas únicas
        const filtroListasSelect = document.getElementById('filtroListas');
        filtroListasSelect.innerHTML = ''; // Limpiar opciones anteriores
        const defaultOption = document.createElement('option');
        defaultOption.value = '*';
        defaultOption.textContent = '*'; // Opción que incluye todas las listas
        filtroListasSelect.appendChild(defaultOption);

        // Convertir el Set a un array y ordenarlo alfabéticamente
        const sortedListas = Array.from(listasUnicas).sort();
        sortedListas.forEach(lista => {
            const option = document.createElement('option');
            option.value = lista;
            option.textContent = lista;
            filtroListasSelect.appendChild(option);
        });

        poblarTablaRegistros(arrayRegistros);
    }).catch((error) => {
        console.error("Error fetching registros: ", error);
    });
}
function poblarTablaRegistros(_arrayRegistros) {
    let listaHTML = "";
    listaHTML += createHeaderLista();
    for (let i = 0; i < _arrayRegistros.length; i++) {
        const registro = _arrayRegistros[i];
        listaHTML += createFilaTabla(registro, i)
    }
    document.getElementById("listaRegistros").innerHTML = listaHTML;

}
function createHeaderLista() {
    let html = "<div class='fila'>"
    html += createCelda("Fecha")
    html += createCelda("nombre")
    html += createCelda("lista")
    html += createCelda("notas")
    html += "</div>"

    return html;
}
function createFilaTabla(registro, _index) {
    let filaHTML = "<div class='fila'>"
    filaHTML += createCeldasFila(registro)

    // filaHTML += createBtnBorrarRegistro(_index);
    // filaHTML += createBtnDescargarCSVRegistro(_index);
    filaHTML += createBtnBorrarRegistro(registro);
    filaHTML += createBtnDescargarCSVRegistro(registro);

    filaHTML += "</div>"
    return filaHTML
}
function createCeldasFila(registro) {
    let html = ""
    html += createCelda(registro.fecha + ' ' + registro.hora)
    html += createCelda(registro.nombre)
    html += createCelda(registro.lista)
    html += createCelda(registro.notas)
    return html;
}
function createCelda(valor) {
    let html = ""
    html += "<div class='celda'>"
    html += valor
    html += "</div>"
    return html
}
// function createBtnBorrarRegistro(_index) {
function createBtnBorrarRegistro(_registro) {
    let fecha = _registro.fecha
    let hora = _registro.hora
    let nombre = _registro.nombre

    let html = ""
    // html += "<div class='btnBorrarRegistro' onclick=window.borrarRegistro(" + _index + ")>X"
    // html += "<div class='btnBorrarRegistro' onclick=window.borrarRegistro('" + fecha + "', ' " + hora + "', '" + nombre + "')>X"
    html += "<div class='btnBorrarRegistro' onclick=\"window.borrarRegistro('"
        + fecha + "', '" + hora + "', '" + nombre + "')\">X";
    // html += "<div class='btnBorrarRegistro' onclick=window.borrarRegistro(" + fecha + ")>X"
    html += "</div>"
    return html
}
function createBtnDescargarCSVRegistro(_registro) {
    let fecha = _registro.fecha
    let hora = _registro.hora
    // let nombre = _registro.nombre

    let html = ""
    // html += "<div class='btnGuardarRegistro' onclick=window.descargarCSV(" + _index + ")>csv"
    html += "<div class='btnGuardarRegistro' onclick=\"window.descargarCSV('"
        + fecha + "', '" + hora + "')\">csv";
    html += "</div>"
    return html
}

////////////////////////////////////////////////////////////
function getRegistroByFechaYHora(fecha, hora) {
    return arrayRegistros.find(registro => registro.fecha === fecha && registro.hora === hora);
}
function descargarCSV(fecha, hora) {
    let registro = getRegistroByFechaYHora(fecha, hora)
    // console.log(registro)
    // return
    // let registro = arrayRegistros[index]
    convertObjectToCsv(registro, registro.nombre + ".csv")
}
window.descargarCSV = descargarCSV
function convertObjectToCsv(obj, csvFileName) {
    if (typeof obj !== 'object' || obj === null) {
        console.error('El objeto debe ser un objeto válido.');
        return;
    }

    const items = obj.items;

    if (!Array.isArray(items) || items.length === 0) {
        console.error('El objeto debe contener un arreglo no vacío llamado "items".');
        return;
    }

    const itemFields = Array.from(new Set(items.flatMap(item => Object.keys(item))));
    const additionalFields = ['fecha', 'hora', 'nombre', 'notas'];
    const headers = [...additionalFields, ...itemFields];

    let csvContent = headers.join(',') + '\n';
    for (const entry of items) {
        // const values = itemFields.map(key => entry[key] !== undefined ? entry[key] : '');
        // const additionalValues = additionalFields.map(field => obj[field] !== undefined ? obj[field] : '');
        // csvContent += [...additionalValues, ...values].join(',') + '\n';
        const values = itemFields.map(key => escapeForCsv(entry[key] !== undefined ? entry[key] : ''));
        const additionalValues = additionalFields.map(field => escapeForCsv(obj[field] !== undefined ? obj[field] : ''));
        csvContent += [...additionalValues, ...values].join(',') + '\n';
    }

    // console.log(csvContent)
    // Crear un objeto Blob para el contenido CSV
    const blob = new Blob([csvContent], { type: 'text/csv' });

    // Crear un enlace de descarga
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = csvFileName;

    // Simular un clic en el enlace para descargar el archivo
    link.click();

    // Liberar recursos
    URL.revokeObjectURL(link.href);
}
function escapeForCsv(value) {
    if (typeof value === 'string') {
        // Escapar comillas dobles y encerrar el valor entre comillas dobles
        return `"${value.replace(/"/g, '""')}"`;
    }
    return value;
}

function borrarRegistro(fecha, hora, nombre) {

    // console.log("borrar: ", id)
    // let fecha = arrayRegistros[id].fecha
    // let hora = arrayRegistros[id].hora
    // let nombre = arrayRegistros[id].nombre
    // console.log(fecha, hora)

    let confirmacion = confirm(`¿Seguro que quiere eliminar el registro de nombre "${nombre}"?`);

    // console.log(fecha, hora, nombre)
    // return

    if (confirmacion) {
        // Si el usuario presiona "Aceptar"
        fb.deleteRegistroByDateAndTime(fecha, hora).then(() => {
            requestLista(); // Actualiza la lista después de eliminar
        });
    } else {
        // Si el usuario presiona "Cancelar"
        console.log("Eliminación cancelada");
    }
}
window.borrarRegistro = borrarRegistro

function filterRegistros() {
    const filtroValue = document.getElementById('filtro').value.toLowerCase();
    const selectedList = document.getElementById('filtroListas').value;

    const filteredRegistros = arrayRegistros.filter(registro => {
        const matchesTextFilter = registro.nombre.toLowerCase().includes(filtroValue) ||
            registro.notas.toLowerCase().includes(filtroValue);
        const matchesListFilter = selectedList === '*' || registro.lista === selectedList;

        return matchesTextFilter && matchesListFilter; // Ambos filtros deben coincidir
    });

    poblarTablaRegistros(filteredRegistros);
}

// Asegúrate de que ambos filtros se escuchen
document.getElementById('filtro').addEventListener('input', filterRegistros);
document.getElementById('filtroListas').addEventListener('change', filterRegistros);