let lista;
let primeraPausa = false;
let segundaPausa = false;
const MENSAJE_MOTIVACIONAL = 310;
const INICIO_LOOP_PRUEBA = 303;
const ESPERA_DE_RESPUESTA = 304;
const CORRECTA = 305;
const INCORRECTA = 306;
const PAUSA = 320;
const PANTALLA_FINAL = 350;
const PAUSA_GENERAL = 500;

let indexItem;


let millisRespuesta = 0
async function setEstadoPresentacion3(estado) {
    if (isPaused) return;


    // console.log("E " + estadoActual)
    switch (estado) {
        case 300:
            primeraPausa = false;
            segundaPausa = false;
            setDuplaImagenes()
            setFondoPresentacion("fondo_1.png")
            // setFondoPresentacion("fondo_2_blur.png")
            addClass("divImgPresentacion1", "big")
            addClass("divImgPresentacion2", "big")
            setPersonaje("exploradora_transicion.png")
            await personajeHablando("Transicion_a.mp3")
            await personajeHablando("Transicion_b.mp3")
            await personajeHablando("Transicion_c.mp3")
            await personajeHablando("Transicion_d.mp3")
            await esperar(1000)

            setEstadoPresentacion(301)
            break
        case 301:
            indexItem = indexItemInicio;

            setFondoPresentacion("fondo_2_blur.png")
            mostrarPersonaje(false)
            setEstadoPresentacion(302)
            break
        case 302:
            /// PRE-CARGAR TODOAS LAS IMAGENES

            // gNombreListaActual = "a"
            lista = gListas[gNombreListaActual]
            // console.log(lista)

            // preloadImagesLista()


            siguienteItemPrueba()
            // setEstadoPresentacion(SIGUIENTE)
            break;
        case 303:
            /// INICIA CICLO PRUEBA
            clearTimeout(timeoutId2);

            // console.log(gRegistro)
            // let currentItem = gListas[gNombreListaActual][indexItem]
            // console.log(currentItem)
            contadorDeEspera = 0;

            // setDuplaImagenes(lista[indexItem].imagen1, lista[indexItem].imagen2)
            setDuplaImagenesOrdenCorrecto(lista[indexItem].imagen1, lista[indexItem].imagen2, lista[indexItem])

            await esperar(1000)

            if (isPaused) return

            if (MODO_DEBUG) {
                console.log(indexItem, lista[indexItem].set)
            }
            await showCortina(true)
            await decirFrase(indexItem)

            if (isPaused) return

            await showCortina(false)

            setEstadoPresentacion(304)
            break;
        case 304:
            /// ESPERA DE RESPUESTA
            console.log("Esperando respuesta...")
            manejarEsperaDeAccion3()

            if (MODO_DEBUG) {
                clickAutomatico()
            }

            break

        case 305:
            /// CORRECTA
            // await mostrarFeedback(POSITIVO)
            siguienteItemPrueba()
            break
        case 306:
            /// INCORRECTA
            // await mostrarFeedback(NEGATIVO)
            siguienteItemPrueba()
            break


        case 310:
            /// MENSAJE MOTIVACIONAL
            setDuplaImagenes()
            showCortina(true)
            await esperar(500)

            showEstrella(true)
            await playAudioGeneral("audios/Msj_motiv_1_341984__unadamlar__winning_CORTADO.mp3")
            await esperar(1000)
            await playMsjMotivacional()
            await esperar(500)
            showEstrella(false)
            showCortina(false)
            await esperar(1000)
            setEstadoPresentacion(INICIO_LOOP_PRUEBA)

            break;

        case 320:
            /// PAUSA (despues de items 20 y 40)
            setDuplaImagenes()
            await esperar(500)
            await esperar(500)
            setPersonaje("exploradora_pausa.png")

            // if(primeraPausa && !segundaPausa){
            // console.log(indexItem)
            if (indexItem < 39) {
                addClass("contador_estrellas_1", "show")
                await personajeHablando("Pausa_1_a.mp3")
                await personajeHablando("Pausa_1_b.mp3")
                await personajeHablando("Pausa_1_c.mp3")
                await personajeHablando("Pausa_1_d.mp3")
            } else {
                addClass("contador_estrellas_2", "show")
                await personajeHablando("Pausa_2_a.mp3")
                await personajeHablando("Pausa_2_b.mp3")
                await personajeHablando("Pausa_2_c.mp3")
                await personajeHablando("Pausa_2_d.mp3")
            }
            addClass("btnDespausar_1", "show")
            setEstadoPresentacion(321);
            break;
        case 321:
            /// ESPERANDO RESPUESTA
            // (al hacer click en el btn "play" se manda al estado 322)
            break;
        case 322:
            /// DESPAUSAR
            removeClass("btnDespausar_1", "show")
            addClass("btnDespausar_2", "show")
            await esperar(500)
            removeClass("btnDespausar_2", "show")
            mostrarPersonaje(false)
            removeClass("contador_estrellas_1", "show")
            removeClass("contador_estrellas_2", "show")

            if (indexItem < 39) {
                setFondoPresentacion("fondo_3_blur.png")
            } else {
                setFondoPresentacion("fondo_5_blur.png")
            }

            siguienteItemPrueba(false)
            // setEstadoPresentacion(INICIO_LOOP_PRUEBA)
            break;

        case 350:
            /// PANTALLA FINAL
            setDuplaImagenes()
            setPersonaje("exploradora_festejando.png")
            // setExpresion("exploradora_muestra_dibujos_cara.png")
            setExpresion("Expresiones/expresion9.png")

            setFondoPresentacion("fondo_4.png")
            addClass("contador_estrellas_3", "show")
            addClass("imgConfeti", "show")

            await playAudioGeneral("audios/Final1_36161__sagetyrtle__bells2_CORTADO.mp3")
            await esperar(500)
            await personajeHablando("Msj_final_a.mp3")
            await personajeHablando("Msj_final_b.mp3")
            await personajeHablando("Msj_final_c.mp3")
            await esperar(2000)
            setEstadoPresentacion(351);

            break;
        case 351:
            /// ocultar todo y reiniciar!
            reiniciarYVolverAlInicio()

            break;

        case 500:
            /// PAUSA GENERAL
            break
        default:
            console.log("ESTADO INEXISTENTE");
            break;
    }
}
function reiniciarYVolverAlInicio() {
    removeClass("contador_estrellas_3", "show")
    removeClass("imgConfeti", "show")
    mostrarPersonaje(false)

    console.log("FIN")

    addClass("pantallaAdmin", "show")
    addClass("menuContainer", "show")
    removeClass("caratulaInicioActividad", "show")
    removeClass("pantallaPresentecionEmparejamiento", "show")

    removeClass("menuActividad", "show")
    isPaused = false;
    showCortina(false)
    removeClass("btnMostrarMenuActividad", "show")


    indexItem = 0;


    window.location.href = '/'

}
let timeoutId2;

async function manejarEsperaDeAccion3() {
    if (isPaused) return

    millisRespuesta = Date.now()
    ultimaRespuesta = "-"

    contadorDeEspera++;

    // Usamos setTimeout
    timeoutId2 = setTimeout(async () => {
        if (isPaused) return
        if (estadoActual == 304) {
            switch (contadorDeEspera) {
                case 1:
                    console.log("mensaje 1");
                    clickAllowed = false;

                    await showCortina(true)
                    await personajeHablando("Practica5_escucha_de_nuevo.mp3")
                    await esperar(500)
                    // await playAudioGeneral(audioEp[practicaIndex])
                    await decirFrase(indexItem)
                    await showCortina(false)

                    if (isPaused) return

                    clickAllowed = true;
                    manejarEsperaDeAccion3();
                    break;
                case 2:
                    console.log("mensaje 2");
                    clickAllowed = false;
                    await showCortina(true)


                    await playAudio("assets/audios/silbido_llamado_atencion.mp3")
                    await personajeHablando("Practica6_estas_ahi_Toca_a.mp3")
                    await personajeHablando("Practica6_estas_ahi_Toca_b.mp3")
                    await showCortina(false)

                    clickAllowed = true;
                    if (isPaused) return

                    manejarEsperaDeAccion3(); // Volvemos a esperar
                    break;
                case 3:
                    console.log("mensaje 3");
                    clickAllowed = false;

                    // showCorrecta(DER)
                    await personajeHablando("Prueba_pasemos_al_siguiente.mp3")
                    // showCorrecta(false)
                    await esperar(1000)

                    clickAllowed = true;
                    if (isPaused) return
                    ultimaRespuesta = "-"
                    contadorDeEspera++;

                    await siguienteItemPrueba()
                    break;
            }
        }
    }, tiempoEsperaDeAccion);
}
//////////////////////////////////////////////////////////////
function clickAutomatico() {
    setTimeout(function () { clickEnImagenEstados300(IZQ) }, 500)
}


var clickAllowed = true;
function clickEnImagenEstados300(index) {
    if (!clickAllowed) {
        return
    }
    if (estadoActual == 304) {
        // var imagen_sin_extension;
        // if (index == IZQ) {
        //     imagen_sin_extension = lista[indexItem].imagen1.replace(".png", "");
        // } else {
        //     imagen_sin_extension = lista[indexItem].imagen2.replace(".png", "");
        // }
        // ultimaRespuesta = imagen_sin_extension
        // if (imagen_sin_extension == lista[indexItem].dibujo_correcto) {
        let correcta = false;
        if (index == IZQ) {
            if (lista[indexItem].ubic_correcta == "izq") {
                correcta = true;
            }
        } else if (index == DER) {
            if (lista[indexItem].ubic_correcta == "der") {
                correcta = true;
            }
        }
        if (correcta) {
            // console.log("CORRECTA")
            ultimaRespuesta = lista[indexItem].dibujo_correcto
            setEstadoPresentacion(CORRECTA)

        } else {
            // console.log("INCORRECTA")
            ultimaRespuesta = lista[indexItem].dibujo_incorrecto.replace("/r", "");

            setEstadoPresentacion(INCORRECTA)

        }
        // if (index == DER) {
        //     setEstadoPresentacion(CORRECTA)
        // } else {
        //     setEstadoPresentacion(INCORRECTA)
        // }
        // }
    }
}

async function siguienteItemPrueba(grabar = true) {
    indexItem++;
    // console.log(indexItem)
    getdom("divIndexItem").innerHTML = indexItem
    prevContadorDeEspera = contadorDeEspera
    contadorDeEspera = 0;

    // console.log(gRegistro)
    // let currentItem = gListas[gNombreListaActual][indexItem]
    // console.log(currentItem)


    if (indexItem >= lista.length) {
        guardarRegistroItem();

        /// ejecuta: fb.setNewRegistro()
        if(!MODO_DEBUG){
            getdom("dummyBtnSaveRegistroDb").click()
        }

        setEstadoPresentacion(PANTALLA_FINAL)
        return
    }

    if (grabar) {
        guardarRegistroItem();
    }

    if (indexItem == 20 && !primeraPausa) {
        indexItem--
        primeraPausa = true;
        setEstadoPresentacion(PAUSA)
        return
    }
    if (indexItem == 40 && !segundaPausa) {
        indexItem--
        segundaPausa = true;
        setEstadoPresentacion(PAUSA)
        return
    }
    // console.log(gRegistro.items)

    // if (indexItem == 5 || indexItem == 12 || indexItem == 27 || indexItem == 35 || indexItem == 49) {
    if (indexItem == 5 || indexItem == 12 || indexItem == 29 || indexItem == 47 || indexItem == 55) {

        setEstadoPresentacion(MENSAJE_MOTIVACIONAL)
    } else {
        setEstadoPresentacion(INICIO_LOOP_PRUEBA)
    }
}
let ultimaRespuesta = -1;
let prevContadorDeEspera = -1;
function guardarRegistroItem() {
    console.log(gListas[gNombreListaActual])
    if (indexItem < 1) return

    let prevItem = gListas[gNombreListaActual][indexItem - 1]
    let newItem = JSON.parse(JSON.stringify(prevItem))

    const fin = Date.now();
    const tiempoDeRespuesta = fin - millisRespuesta;
    // console.log(tiempoDeRespuesta)

    newItem.respuesta = ultimaRespuesta
    newItem.recordatorios = prevContadorDeEspera - 1;

    if (ultimaRespuesta == "-") {
        newItem.tiempoDeRespuesta = "-";
    } else {
        newItem.tiempoDeRespuesta = tiempoDeRespuesta;
    }

    let orden = 0;
    if (gRegistro.items.length > 0) orden = gRegistro.items.length
    newItem.orden = orden;


    gRegistro.items.push(newItem)
    // console.log(gRegistro.items)
    // console.log(JSON.stringify(newItem))

}
/////////////////////////////////////////////
function showEstrella(show) {
    if (show) {
        addClass("imgEstrellaSuelta", "show")
    } else {
        removeClass("imgEstrellaSuelta", "show")
    }
}

///////////////////////////////////////////////////
let msjMotivacionalIndex = -1;
let audiosMsjMotivacional = [
    "Msj_motivacional_1.mp3",
    "Msj_motivacional_2.mp3",
    "Msj_motivacional_3.mp3",
    "Msj_motivacional_4.mp3",
    "Msj_motivacional_3.mp3"

]
async function playMsjMotivacional() {
    msjMotivacionalIndex++;
    await personajeHablando(audiosMsjMotivacional[msjMotivacionalIndex])
}
/////////////////////////////////
getdom("btnDespausar_1").onclick = () => { despausar() }
getdom("btnDespausar_2").onclick = () => { despausar() }
function despausar() {
    if (estadoActual != 321) {
        return
    } else {
        setEstadoPresentacion(322)
    }
}

///////////////////////////
let estadoPrevioAPausa = -1;
let isPaused = false;

function pausarActividad(_pausar) {
    // isPaused = !isPaused;
    clearTimeout(timeoutId2);

    isPaused = _pausar;
    if (isPaused) {
        // console.log("------------------------------")
        // console.log("Estado actual: ", estadoActual)

        estadoPrevioAPausa = estadoActual
        if (estadoActual == ESPERA_DE_RESPUESTA) {
            estadoPrevioAPausa = INICIO_LOOP_PRUEBA
        }
        showCortina(true)
        // addClass("btnPausa", "visible")
        addClass("menuActividad", "show")

        if (audioElement) {
            audioElement.pause(); // Pausa el audio
            audioElement.currentTime = 0; // Opcional: reinicia el audio al principio
        }
        // console.log("PAUSAR")
        setEstadoPresentacion(PAUSA_GENERAL);
    } else {
        // console.log("Estado previo: ", estadoPrevioAPausa)
        showCortina(false)
        removeClass("menuActividad", "show")



        // console.log("DES-PAUSAR")
        // millisRespuesta = Date.now()
        setEstadoPresentacion(estadoPrevioAPausa);
    }
}

let isFullscreen = false;
function activarPantallaCompleta(activate = true, force = false) {
    if (!force) {
        if (!ENABLE_FULLSCREEN) return;
    }
    let elemento = document.documentElement;  // Obtiene el elemento <html>

    if (activate) {
        if (elemento.requestFullscreen) {
            elemento.requestFullscreen();
        } else if (elemento.mozRequestFullScreen) {  /* Firefox */
            elemento.mozRequestFullScreen();
        } else if (elemento.webkitRequestFullscreen) {  /* Chrome, Safari & Opera */
            elemento.webkitRequestFullscreen();
        } else if (elemento.msRequestFullscreen) {  /* IE/Edge */
            elemento.msRequestFullscreen();
        }
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {  /* Firefox */
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {  /* Chrome, Safari & Opera */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {  /* IE/Edge */
            document.msExitFullscreen();
        }
    }
}
function isInFullScreen() {
    return !!(document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement);
}


let wakeLock = null;

// Función para solicitar el bloqueo de pantalla
const requestWakeLock = async () => {
    try {
        wakeLock = await navigator.wakeLock.request('screen');
        wakeLock.addEventListener('release', () => {
            // console.log('Wake Lock fue liberado');
        });
        // console.log('Wake Lock está activo');
    } catch (err) {
        console.error(`No se pudo obtener el wake lock: ${err.name}, ${err.message}`);
    }
}

/////////////////////////////////////////////