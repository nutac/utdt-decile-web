/// midi
if (navigator.requestMIDIAccess) {
    console.log("This browser supports WebMIDI!");
    navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
} else {
    console.log("WebMIDI is not supported in this browser.");
}

//   var launchpadOutput = -1;
var midiOut = -1;
function onMIDISuccess(midiAccess) {
    //   console.log(midiAccess);
    var inputs = midiAccess.inputs;
    // console.log("INPUTS:", inputs);
    //   console.log(midiAccess.inputs.values());
    for (var input of midiAccess.inputs.values()) {
        // console.log(input);
        input.onmidimessage = getMIDIMessage;
    }

    //   var outputs = midiAccess.outputs;
    //   console.log("OUTPUTS:", outputs);
    //   for (let i = 0; i < outputs.length; i++) {
    for (var output of midiAccess.outputs.values()) {
        // let output = outputs[i].values();
        console.log(output);
        if (output.name) {
            //   console.log(output.name.toString().toLowerCase());
            if (output.name.toLowerCase().includes("loopbe")) {
                // console.log("Launchpad conectado:", output);
                //   console.log("Launchpad conectado");
                //   launchpadOutput = output;
                midiOut = output;
                //   resetLeds();
                break;
            }
        }
    }

}

function onMIDIFailure() {
    console.log("Could not access your MIDI devices.");
}

function getMIDIMessage(midiMessage) {
    //   console.log(midiMessage);
    let data = midiMessage.data;
    // launchpadPressed(data);
    // uiController(data);

    //   console.log(data);
    let cmd = data[0] >> 4;
    let channel = data[0] & 0xf;
    let type = data[0] & 0xf0;
    let note = data[1];
    let velocity = data[2];


    if (note == 60) {
        if (velocity == 127) {
            document.getElementById("estadoAudio").classList.add("recording")
            startRecording()
        } else if (velocity == 0) {
            document.getElementById("estadoAudio").classList.remove("recording")
            stopRecording()
        }
    }
}

function sendNoteTo(output, note, velocity = 127, duration = 0) {
    let noteOn = 0x90;
    let noteOff = 0x80;
    output.send([noteOn, note, velocity]);
    //   output.send([0x80, note, velocity], window.performance.now() + duration);
}
var mediaRecorder;
var stream;
var chunks;
const setupRecording = async () => {

    // const utterance = new SpeechSynthesisUtterance();
    let voices = window.speechSynthesis.getVoices()
    console.log(voices)
    // utterance.lang = 'es-ES';

    stream = await navigator.mediaDevices.getUserMedia({ audio: true });

    chunks = [];
    mediaRecorder = new MediaRecorder(stream);

    mediaRecorder.addEventListener('dataavailable', e => {
        chunks.push(e.data);
    });

    mediaRecorder.addEventListener('stop', () => {
        const blob = new Blob(chunks, { type: 'audio/webm' });

        downloadBlob(blob)

    });
}
function downloadBlob(blob) {
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    // a.download = 'audio.webm';
    // let filename = preguntaToFilename(getPregunta(indexPregunta))
    let filename = "esto es una banana"
    console.log(filename)
    // a.download = filename + '.webm';
    a.download = filename + '.webm';


    document.body.appendChild(a);
    a.click();
}

setupRecording();

const startRecording = async () => {
    chunks = []
    chunks.length = 0

    // utterance.text = document.getElementById('textInput').value;


    // incPreguntaIndex();
    // setOracionSelected(indexPregunta)

    mediaRecorder.start();
    // speechSynthesis.speak(utterance);

    //   utterance.onend = () => {
    // setTimeout(function () {

    //     mediaRecorder.stop();
    // },
    //     4000)
    //   };
};
const stopRecording = async () => {
    mediaRecorder.stop();

}