const buttonPlay = document.querySelector("#play");
const buttonDownload = document.querySelector("#download");
const textInput = document.querySelector("#text");
let audio;

// Create audio context
const audioContext = new (window.AudioContext || window.webkitAudioContext)();

// Create a function to play text-to-speech
function textToSpeech() {
    const utterance = new SpeechSynthesisUtterance(textInput.value || "hola");
    const voices = window.speechSynthesis.getVoices();
    utterance.voice = voices[0];
    utterance.onend = () => {
        // Create a media element source node
        const sourceNode = audioContext.createMediaElementSource(audio);
        // Create a script processor node to process audio data
        const scriptProcessorNode = audioContext.createScriptProcessor(2048, 1, 1);
        // Create a media recorder instance
        const mediaRecorder = new MediaRecorder(audioContext.destination, {
            mimeType: "audio/wav"
        });

        // Connect the nodes and start recording
        sourceNode.connect(scriptProcessorNode);
        scriptProcessorNode.connect(audioContext.destination);
        mediaRecorder.start();

        // When data is available, push it to chunks array
        const chunks = [];
        scriptProcessorNode.onaudioprocess = (event) => {
            const inputBuffer = event.inputBuffer;
            const inputData = inputBuffer.getChannelData(0);
            chunks.push(inputData);
        };

        // When recording is stopped, create a blob and download it
        mediaRecorder.onstop = () => {
            const audioBlob = new Blob(chunks, {
                type: "audio/wav"
            });
            const audioUrl = URL.createObjectURL(audioBlob);
            const link = document.createElement("a");
            link.href = audioUrl;
            link.download = "text-to-speech.wav";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        };

        // Stop recording after 2 seconds
        setTimeout(() => {
            mediaRecorder.stop();
        }, 2000);
    };
    speechSynthesis.speak(utterance);
}

// Add event listeners to buttons
buttonPlay.addEventListener("click", () => {
    textToSpeech();
});
buttonDownload.addEventListener("click", () => {
    audio.pause();
});

// Preload audio element with silent audio
audio = new Audio();
audio.src = "data:audio/wav;base64,UklGRpAAAABXQVZFZm10IBAAAAABAAEAQB8AAAAAAAIAAAAgTAAACABAAAABBxZG9iZV9DTQAOAAAAHAAAAC8BAAAwAQAAFgAAACYCdAA=";