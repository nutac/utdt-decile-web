const utterance = new SpeechSynthesisUtterance();
let voices = window.speechSynthesis.getVoices()

// console.log(window.speechSynthesis.getVoices())
utterance.lang = 'es-AR';



/// MIDI
if (navigator.requestMIDIAccess) {
    console.log("This browser supports WebMIDI!");
    navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
} else {
    console.log("WebMIDI is not supported in this browser.");
}

var midiOut = -1;
function onMIDISuccess(midiAccess) {
    //   console.log(midiAccess);
    var inputs = midiAccess.inputs;
    console.log("INPUTS:", inputs);
    //   console.log(midiAccess.inputs.values());
    for (var input of midiAccess.inputs.values()) {
        // console.log(input);
        input.onmidimessage = getMIDIMessage;
    }

    //   var outputs = midiAccess.outputs;
    //   console.log("OUTPUTS:", outputs);
    //   for (let i = 0; i < outputs.length; i++) {
    for (var output of midiAccess.outputs.values()) {
        // let output = outputs[i].values();
        // console.log(output);
        if (output.name) {
            //   console.log(output.name.toString().toLowerCase());
            if (output.name.toLowerCase().includes("loopbe")) {
                // console.log("Launchpad conectado:", output);
                //   console.log("Launchpad conectado");
                midiOut = output;
                //   resetLeds();
                break;
            }
        }
    }

}
function getMIDIMessage(midiMessage) {
    // //   console.log(midiMessage);
    // let data = midiMessage.data;
    // // launchpadPressed(data);
    // // uiController(data);

    // //   console.log(data);
    // let cmd = data[0] >> 4;
    // let channel = data[0] & 0xf;
    // let type = data[0] & 0xf0;
    // let note = data[1];
    // let velocity = data[2];
    // console.log(
    //     //   "cmd: ",
    //     //   cmd,
    //     //   ", channel: ",
    //     //   channel,
    //     //   ", type: " + type,
    //     "note:",
    //     note,
    //     ", velocity: " + velocity
    // );
}
function onMIDIFailure() {
    console.log("Could not access your MIDI devices.");
}


const playAndRecord = async () => {
    // const stream = await navigator.mediaDevices.getUserMedia({ audio: true });

    // const mediaRecorder = new MediaRecorder(stream);
    // const chunks = [];

    // mediaRecorder.addEventListener('dataavailable', e => {
    //     chunks.push(e.data);
    // });

    // mediaRecorder.addEventListener('stop', () => {
    //     const blob = new Blob(chunks, { type: 'audio/webm' });
    //     const url = URL.createObjectURL(blob);
    //     const a = document.createElement('a');
    //     a.href = url;
    //     a.download = 'audio.webm';
    //     document.body.appendChild(a);
    //     a.click();
    // });

    // console.log(window.speechSynthesis.getVoices())
    // utterance.voice = window.speechSynthesis.getVoices()[224]

    utterance.text = document.getElementById('textInput').value;

    console.log("inicio")

    // incPreguntaIndex();
    // let pregunta = getPregunta(indexPregunta)
    // if (pregunta == -1) return
    // console.log(pregunta);
    // setOracionSelected(indexPregunta)
    // utterance.text = pregunta


    // mediaRecorder.start();
    speechSynthesis.speak(utterance);

    // console.log(midiOut)
    let noteOn = 0x90;
    let noteOff = 0x80;
    midiOut.send([noteOn, 60, 127]);

    utterance.onend = () => {
        console.log("fin")
        midiOut.send([noteOff, 60, 0]);

        // setTimeout(function () {
        //     playAndRecord()
        // }, 1000)

        // setTimeout(function () {

        // mediaRecorder.stop();
        // },
        // 4000)
    };
};

document.getElementById('playAndRecordButton').addEventListener('click', playAndRecord);