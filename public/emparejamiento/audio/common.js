let preguntas = []
let indexPregunta = -1;
setPreguntas();
function setPreguntas() {
    // preguntas.push("¿Quién encadena al ladrón?")
    // preguntas.push("¿A quién despeina el pintor?")
    // preguntas.push("¿Qué mago dibuja al capitán?")
    // preguntas.push("¿A qué zorro olfatea el león?")
    // preguntas.push("¿Quién empuja a la policía?")
    // preguntas.push("¿A quién acaricia la bruja?")
    // preguntas.push("¿Qué maestra sopla a la bailarina?")
    // preguntas.push("¿A qué cocinero señala el abuelo?")
    // preguntas.push("¿Quién agarra al médico?")
    // preguntas.push("¿A quién ata el príncipe?")
    // preguntas.push("¿Qué lobo toca al pájaro?")
    // preguntas.push("¿A qué lobo toca el pájaro?")
    preguntas = oraciones;

}

function incPreguntaIndex() {
    indexPregunta++;
    if (indexPregunta >= preguntas.length) {
        console.log("fin del array de preguntas!")
        indexPregunta = -1
    }
}
function getPregunta(_index) {
    if (_index > -1) {
        return preguntas[indexPregunta]
    } else {
        return -1
    }

}


function fraseToFilename(str) {
    str = str.replace(/\?/g, '');
    str = str.replace(/\s+/g, '_');
    // Reemplaza los acentos por letras sin acentos
    str = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    str = str.replace(/ñ/g, 'n');
    // Elimina cualquier caracter que no sea una letra, un número, un guión bajo, un punto o un guión medio
    str = str.replace(/[^\w.-]/g, '');
    return str;
}


////////////////////
// createDivOraciones()
function createDivOraciones() {
    let html = "";
    let i = 0
    oraciones.forEach(oracion => {
        // console.log(i, oracion)
        i++
        html += createDivOracion(i, oracion)
    });
    document.getElementById("listaDeOraciones").innerHTML = html;

    setOracionSelected(1)
}
function createDivOracion(i, oracion) {
    let html = ""
    html += "<div class='fila'>"
    html += i + " - "
    html += oracion

    html += "</div>"
    return html;
}
function setOracionSelected(_index) {
    let filas = document.getElementsByClassName("fila")
    for (let i = 0; i < filas.length; i++) {
        if (i == _index) {
            filas[i].classList.add("selected")
        } else {
            filas[i].classList.remove("selected")

        }
    }
}