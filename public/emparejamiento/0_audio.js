
function getOracion(_index, _list) {
    return _list[_index].oracion
}
const decirFrase = async (_indexItem) => {
    let frase = getOracion(_indexItem, gListas[gNombreListaActual])
    // let path = "/assets/emparejamiento/audios/"
    let path = "../data/data/emparejamiento/oraciones/";


    // let archivo = path + fraseToFilename(frase) + ".webm"
    let archivo = path + fraseToFilename(frase) + ".mp3"

    // console.log(archivo)
    // if (!MODO_DEBUG) {
        await playAudio(archivo) // Esperar a que termine la reproducción del audio
    // } else {
        // console.log("FRASE: " + frase)
    // }
};
let audioElement;
const playAudio = async (audioFile, loop = false) => {
    if (isPaused) {
        return
    }
    return new Promise((resolve) => {
        console.log(audioFile)
        audioElement = new Audio(audioFile);
        if (loop) {
            audioElement.addEventListener('ended', () => {
                audioElement.currentTime = 0; // Reiniciar el audio al finalizar
                audioElement.play(); // Volver a reproducir en bucle
            });
        } else {
            audioElement.addEventListener('ended', () => {
                resolve(); // Resolver la promesa cuando termine la reproducción del audio
            });
        }
        audioElement.play(); // Reproducir el archivo de audio
    });
};
function fraseToFilename(str) {
    str = str.replace(/\?/g, '');
    str = str.replace(/\s+/g, '_');
    // Reemplaza los acentos por letras sin acentos
    str = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    str = str.replace(/ñ/g, 'n');
    // Elimina cualquier caracter que no sea una letra, un número, un guión bajo, un punto o un guión medio
    str = str.replace(/[^\w.-]/g, '');
    return str;
}

////////////////////
var allSoundsLoaded;
var totalSounds;
var soundsLoaded;
async function preloadSoundFiles() {
    gNombreListaActual = getdom("comboListas").value
    if (gNombreListaActual == "-") {
        console.log("debe seleccionar una LISTA")
        // return
    } else {
        allSoundsLoaded = false;
        totalSounds = gListas[gNombreListaActual].length
        soundsLoaded = 0;
        addClass("loader", "show")
        loadSounds(gListas[gNombreListaActual])
    }
}
// async function loadSounds(_allItems) {
//     totalSounds = _allItems.length
//     let path = "assets/emparejamiento/audios/"

//     for (let i = 0; i < _allItems.length; i++) {
//         let frase = getOracion(i, gListas[gNombreListaActual])
//         let soundFile = path + fraseToFilename(frase) + ".mp3"
//         // console.log(i, soundFile)

//         const myAudio1 = new Audio();
//         myAudio1.src = soundFile;
//         myAudio1.addEventListener('canplaythrough', audioLoaded);  // 'canplaythrough' se dispara cuando el audio se ha cargado y está listo para ser reproducido
//         myAudio1.addEventListener('error', audioError);  // En caso de error
//     }
// }
async function loadSounds(_allItems) {
    totalSounds = _allItems.length;
    // let path = "/assets/emparejamiento/audios/";
    let path = "../data/data/emparejamiento/oraciones/";


    for (let i = 0; i < _allItems.length; i++) {
        let frase = getOracion(i, gListas[gNombreListaActual]);
        let soundFile = path + fraseToFilename(frase) + ".mp3";

        // Usar la nueva función para cargar el audio completamente
        const audioURL = await loadAudioFully(soundFile);
        if (audioURL) {
            const myAudio1 = new Audio();
            myAudio1.src = audioURL;
            myAudio1.addEventListener('canplaythrough', audioLoaded);
            myAudio1.addEventListener('error', audioError);
        } else {
            // Manejar el error si no se pudo cargar el audio
            audioError({ target: { src: soundFile } });
        }
    }
}

async function audioLoaded(event) {
    // console.log("Audio cargado:", event.target.src);
    // console.log("Audio cargado:", event.target.src);

    soundsLoaded++;

    let pct = Math.round(soundsLoaded * 100 / totalSounds);
    getdom("loaderPctSonidos").innerHTML = pct + " %"



    if (soundsLoaded >= totalSounds) {
        allSoundsLoaded = true
        finishLoadingAllAssets()
    }
}

function audioError(event) {
    // console.error("Error al cargar el audio:", event.target.src);
    let fullPath = event.target.src;
    let fileName = new URL(fullPath).pathname.split('/').pop();
    console.log("No se encuentra el audio: " + fileName)
}

async function loadAudioFully(url) {
    try {
        const response = await fetch(url);
        const audioData = await response.blob();
        return URL.createObjectURL(audioData);
    } catch (error) {
        console.error("Error al cargar el audio:", error);
        return null;
    }
}
