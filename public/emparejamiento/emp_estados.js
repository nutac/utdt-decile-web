
var estadoActual = -1;
const SIGUIENTE = -2;
async function setEstadoPresentacion(estado) {
    if (isPaused) return;

    if (estado == SIGUIENTE) {
        // estadoActual++
        estado = estadoActual + 1
        console.log("SIGUIENTE")
    } else {
    }
    estadoActual = estado
    if (estado >= 200) {
        setEstadoPresentacion2(estado);
        return
    }
    console.log("E " + estadoActual)

    switch (estado) {
        case 0:
            // console.log(gRegistro)
            console.log("hola")

            setPersonaje("exploradora_pausa.png")
            animarOjos(true, 0)
            await playAudioGeneral("audios/Musica_inicio_639933__sergmusic__blue-sky_CORTADO.mp3")
            animarOjos(false, 0)
            
            setPersonaje("exploradora_saludando.png")
            console.log("hola 2")
            await personajeHablando("Presentacion_a.mp3")
            
            console.log("hola 3")

            // await esperar(500);
            await personajeHablando("Presentacion_b.mp3")
            // await esperar(500);
            await personajeHablando("Presentacion_c.mp3")
            // await esperar(500);
            await personajeHablando("Presentacion_d.mp3")
            // await esperar(500);

            setPersonaje("exploradora_pausa.png")

            await esperar(1000);
            setEstadoPresentacion(SIGUIENTE);
            console.log("chau")

            break;
        case 1:
            setPersonaje("exploradora_muestra_dibujos_cuerpo.png")
            setExpresion("exploradora_muestra_dibujos_cara.png")

            setDuplaImagenes("ilustraciones/Otros/manzana1.png", "ilustraciones/Otros/banana1.png")
            // setImagenesPresentacion("emparejamiento/dibujos/EP1_B_cantante_pincha_tenista.png",
            // "ilustraciones/Otros/banana1.png")
            await personajeHablando("Instrucciones1mono_a.mp3")
            // await esperar(500);
            await personajeHablando("Instrucciones1mono_b.mp3")
            // await esperar(500);
            await personajeHablando("Instrucciones1mono_c.mp3")

            await esperar(500);

            // await personajeHablando("esto_es_una_banana.mp3")
            // await playAudioGeneral("audios/audios_personaje/esto_es_una_banana.mp3")
            await playAudioGeneral("Esto_es_una_banana.mp3")

            // await esperar(1000);
            await esperar(500);

            await personajeHablando("Instrucciones2mono_a.mp3")
            // await esperar(500);
            await personajeHablando("Instrucciones2mono_b.mp3")

            await esperar(500);
            setEstadoPresentacion(SIGUIENTE);
            break;

        case 2:
            /// esperando respuesta de click

            break;
        case 3:
            /// respuesta incorrecta
            mostrarFeedback(NEGATIVO)
            setEstadoPresentacion(5)
            break;
        case 4:
            /// respuesta correcta
            mostrarFeedback(POSITIVO)
            setEstadoPresentacion(5)
            break;
        case 5:
            // await esperar(2000);

            await mostrarFeedback(false)

            // await esperar(1000)
            setEstadoPresentacion(6)
            break;
        case 6:
            /// MOSTRAR LA IMAGEN CORRECTA
            setPersonaje("exploradora_senala_derecha_cuerpo.png")
            // setExpresion("exploradora_senala_derecha_cara.png")
            showCorrecta(DER)
            await personajeHablando("Instrucciones3mono_a.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones3mono_b.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones3mono_c.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones3mono_d.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones3mono_e.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones3mono_f.mp3")
            // await esperar(500)
            setEstadoPresentacion(SIGUIENTE);

            break;
        case 7:
            /// TRANSICION
            setPersonaje("exploradora_pausa.png")
            setExpresion("Expresiones/expresion14.png")
            await esperar(500)
            hideDuplaImagenes()
            await esperar(500)
            setEstadoPresentacion(SIGUIENTE);
            break;
        case 8:

            showCorrecta(false)

            // setDuplaImagenes("emparejamiento/dibujos/EP1_A_tenista_pincha_cantante.png", "emparejamiento/dibujos/EP1_B_cantante_pincha_tenista.png")
            setDuplaImagenes("EP1_A_tenista_pincha_cantante.png", "EP1_B_cantante_pincha_tenista.png")

            await personajeHablando("Instrucciones4mono_a.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones4mono_b.mp3")


            await esperar(500)
            await playAudioGeneral("la_tenista_pincha_a_la_cantante.mp3")
            // await esperar(500);

            setPersonaje("exploradora_muestra_dibujos_cuerpo.png")
            setExpresion("exploradora_muestra_dibujos_cara.png")

            await personajeHablando("Instrucciones5mono_a.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones5mono_b.mp3")
            // await esperar(500)

            setEstadoPresentacion(SIGUIENTE);
            break;
        case 9:
            /// esperando respuesta de click

            break;
        case 10:
            /// respuesta incorrecta
            console.log("INCORRECTO")
            await mostrarFeedback(NEGATIVO)
            setEstadoPresentacion(12)
            break;
        case 11:
            /// respuesta correcta
            console.log("CORRECTO")
            await mostrarFeedback(POSITIVO)
            setEstadoPresentacion(12)
            break;
        case 12:
            // await esperar(2000);
            // mostrarFeedback(false)
            // await esperar(1000)
            setEstadoPresentacion(SIGUIENTE);
            break;
        case 13:
            setPersonaje("exploradora_senala_izquierda_cuerpo.png")
            // setExpresion("exploradora_muestra_dibujos_cara.png")
            console.log("izquierda!")
            showCorrecta(IZQ)
            await personajeHablando("Instrucciones6mono_a.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones6mono_b.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones6mono_c.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones6mono_d.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones6mono_e.mp3")
            // await esperar(500)
            await personajeHablando("Instrucciones6mono_f.mp3")
            // await esperar(500)
            showCorrecta(false)
            // await esperar(1000)
            await esperar(500)

            /// A VER, VAMOS A PRACTICAR
            mostrarPersonaje(false)
            setDuplaImagenes();
            await personajeHablando("Practica1_vamos_a_practicar.mp3")
            // await playAudio2(
            //     "assets/audios/audios_personaje/Practica1_vamos_a_practicar.mp3",
            //     2
            // )
            await esperar(1000)


            setEstadoPresentacion(SIGUIENTE);
            break;
        case 14:



            addClass("divImgPresentacion1", "big")
            addClass("divImgPresentacion2", "big")
            setEstadoPresentacion(200);
            break;
        default:
            console.log("ESTADO INEXISTENTE");
            break;
    }
}

