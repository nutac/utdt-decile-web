// import { deleteRegistroByFecha } from "./0_bd_firebase"

var gRegistro = {}

// const pathDibujos = "/assets/emparejamiento/dibujos2/"
const pathDibujos = "../data/data/emparejamiento/dibujos/"


getdom("btnEmpezar").addEventListener("click", async function () {
    await crearRegistroYMostrarActividad()
})

document.getElementById('pass').addEventListener('input', function () {
    let val = this.value.toLowerCase()
    // let word = 'torcuato'
    // if (modalRegistros.showModal) 
    let word = 'ditella'
    if (val == word) {
        // utils.showPassword(false);
        removeClass("containerPassword", "show")
    }
});

document.getElementById('filtro').addEventListener('input', function () {
    const filtroTexto = this.value.toLowerCase();
    const filtroListaTexto = document.getElementById('filtroListas').value.toLowerCase();
    poblarTablaRegistros(filtroTexto, filtroListaTexto);
});

document.getElementById('filtroListas').addEventListener('input', function () {
    const filtroTexto = document.getElementById('filtro').value.toLowerCase();
    const filtroListaTexto = this.value.toLowerCase();
    poblarTablaRegistros(filtroTexto, filtroListaTexto);
});

async function crearRegistroYMostrarActividad() {
    gRegistro = {}

    if (!MODO_DEBUG) {
    }
    shuffleObjectArrays(gListas)


    let nombre = getdom("inpNombre").value
    let notas = getdom("inpNotas").value
    let tiempo = getdom("inpTiempo").value
    // let tiempo = 3000;
    let lista = getdom("comboListas").value


    gRegistro = {
        nombre: nombre,
        notas: notas,
        tiempo: tiempo,
        lista: lista,
        items: [],
    }

    tiempoEsperaDeAccion = tiempo
    duracionRecordatorio = tiempo

    let status = checkCampos(gRegistro)
    if (status.ok) {
        const date = new Date();
        gRegistro.fecha = date.toLocaleString()
        // console.log(gRegistro)
        // console.log("Todo OK!")
        preloadSoundFiles();

        preloadImagesBasedOnComboListas()
        preloadBaseFiles();


        // addClass("pantallaActividad", "show")
        removeClass("pantallaAdmin", "show")
        removeClass("menuContainer", "show")

        addClass("btnEscape0", "show")

        addClass("caratulaInicioActividad", "show")

        getdom("dummyBtnSaveTiempoDb").click()


        // console.log("gRegistro:", gRegistro)

    } else {
        // console.log(status.error)
        alert(status.error)
    }


    function checkCampos(reg) {
        let isOk = true;
        let err = ""
        if (reg.nombre == "") {
            err = "Falta nombre"
        }
        else if (reg.tiempo == "") {
            // err = "Listas no cargadas"
            err = 'Esperando respuesta de BD para leer variable TIEMPO. \n Vuelva a intentarlo'
            if (MODO_DEBUG) {
                err = ""
                reg.tiempo = 4000
            }
        } else if (reg.lista == "-") {
            err = "seleccionar una lista!"
        }
        if (err != "") isOk = false

        return { ok: isOk, error: err }
    }
}
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];  // Intercambio de elementos usando desestructuración
    }
    return array;
}
function shuffleObjectArrays(obj) {
    for (let key in obj) {
        if (Array.isArray(obj[key])) {
            obj[key] = shuffleArray(obj[key]);
        }
    }
    return obj;
}
///////////////////////////////////////////////////////


let arrayRegistros = []

function poblarTablaRegistros(filtroTexto = "", filtroListaTexto = "") {
    let _arrayRegistros = arrayRegistros;
    let listaHTML = "";
    listaHTML += createHeaderLista();
    for (let i = 0; i < _arrayRegistros.length; i++) {
        const registro = _arrayRegistros[i];
        // Filtrar registros según el texto ingresado en ambos inputs
        if ((registro.nombre.toLowerCase().includes(filtroTexto) ||
            registro.notas.toLowerCase().includes(filtroTexto)) &&
            (filtroListaTexto === "*" || registro.lista.toLowerCase().includes(filtroListaTexto))) {
            listaHTML += createFilaTabla(registro, i);
        }
    }
    document.getElementById("listaRegistros").innerHTML = listaHTML;
}
function createHeaderLista() {
    let html = "<div class='fila'>"
    html += createCelda("Fecha")
    html += createCelda("nombre")
    html += createCelda("lista")
    html += createCelda("notas")
    html += "</div>"

    return html;
}
function createFilaTabla(registro, _index) {
    let filaHTML = "<div class='fila'>"
    filaHTML += createCeldasFila(registro)
    filaHTML += createBtnBorrarRegistro(_index);
    filaHTML += createBtnGuardarRegistro(_index);
    filaHTML += "</div>"
    return filaHTML
}
function createCeldasFila(registro) {
    let html = ""
    html += createCelda(registro.fecha)
    html += createCelda(registro.nombre)
    html += createCelda(registro.lista)
    html += createCelda(registro.notas)
    return html;
}
function createCelda(valor) {
    let html = ""
    html += "<div class='celda'>"
    html += valor
    html += "</div>"
    return html
}
function createBtnBorrarRegistro(_index) {
    let html = ""
    html += "<div class='btnBorrarRegistro' onclick=borrarRegistro(" + _index + ")>X"
    html += "</div>"
    return html
}
function createBtnGuardarRegistro(_index) {
    let html = ""
    html += "<div class='btnGuardarRegistro' onclick=guardarRegistro(" + _index + ")>csv"
    html += "</div>"
    return html
}
function borrarRegistro(_index) {
    let registro = arrayRegistros[_index];
    let fecha = registro.fecha;
    let nombre = registro.nombre;

    const respuesta = confirm(`¿Estás seguro de eliminar el registro: ${fecha} - ${nombre}?`);

    if (respuesta) {
        deleteRegistroByFecha(fecha);
    } else {
        console.log("Acción cancelada por el usuario.");
    }
}

function guardarRegistro(_index) {
    let registro = arrayRegistros[_index]
    // console.log(JSON.stringify(registro))
    // console.log(registro)
    convertObjectToCsv(registro, registro.nombre + ".csv")
    // saveObjectToFile(JSON.stringify(registro), 'registro.json', 'text/plain');
}

function saveObjectToFile(content, fileName, contentType) {
    var a = document.createElement("a");
    var file = new Blob([content], { type: contentType });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

function convertObjectToCsv(obj, csvFileName) {
    if (typeof obj !== 'object' || obj === null) {
        console.error('El objeto debe ser un objeto válido.');
        return;
    }

    const items = obj.items;

    if (!Array.isArray(items) || items.length === 0) {
        console.error('El objeto debe contener un arreglo no vacío llamado "items".');
        return;
    }

    const itemFields = Array.from(new Set(items.flatMap(item => Object.keys(item))));
    const additionalFields = ['fecha', 'nombre', 'notas', 'tiempo'];
    const headers = [...additionalFields, ...itemFields];

    let csvContent = headers.join(',') + '\n';
    for (const entry of items) {
        // const values = itemFields.map(key => entry[key] !== undefined ? entry[key] : '');
        // const additionalValues = additionalFields.map(field => obj[field] !== undefined ? obj[field] : '');
        // csvContent += [...additionalValues, ...values].join(',') + '\n';
        const values = itemFields.map(key => escapeForCsv(entry[key] !== undefined ? entry[key] : ''));
        const additionalValues = additionalFields.map(field => escapeForCsv(obj[field] !== undefined ? obj[field] : ''));
        csvContent += [...additionalValues, ...values].join(',') + '\n';


    }

    // console.log(csvContent)
    // Crear un objeto Blob para el contenido CSV
    const blob = new Blob([csvContent], { type: 'text/csv' });

    // Crear un enlace de descarga
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = csvFileName;

    // Simular un clic en el enlace para descargar el archivo
    link.click();

    // Liberar recursos
    URL.revokeObjectURL(link.href);
}
function escapeForCsv(value) {
    if (typeof value === 'string') {
        // Escapar comillas dobles y encerrar el valor entre comillas dobles
        return `"${value.replace(/"/g, '""')}"`;
    }
    return value;
}


//// PRE-LOAD ASSETS (IMAGES AND SOUNDS) ////////
async function preloadImagesBasedOnComboListas() {
    // console.log("RESET")
    // gIndexItem = -1;

    gNombreListaActual = getdom("comboListas").value
    preloadImagesLista(gNombreListaActual)
}
async function preloadImagesLista() {
    if (gNombreListaActual == "-") {
        console.log("debe seleccionar una LISTA")
        // return
    } else {
        addClass("loader", "show")
        allImagesLoaded = false;
        loadImages(gListas[gNombreListaActual])
        // loadSounds(gListas[gNombreListaActual])
    }
}

async function loadImages(_allItems) {

    totalImages = _allItems.length * 2
    totalImages += imagesBaseFilesUrls.length;

    // let path = "/assets/emparejamiento/dibujos/"
    let path = pathDibujos
    for (let i = 0; i < _allItems.length; i++) {
        let url1 = path + _allItems[i].imagen1;
        let url2 = path + _allItems[i].imagen2;

        const myImage1 = new Image(50, 50);
        myImage1.src = url1;
        // console.log(url1)
        getdom("dummyImages").appendChild(myImage1);

        const myImage2 = new Image(50, 50);
        myImage2.src = url2;
        getdom("dummyImages").appendChild(myImage2);
        myImage1.addEventListener('load', imgLoaded);
        myImage2.addEventListener('load', imgLoaded);
        myImage1.addEventListener('error', function () {
            console.error('Error loading image:', url1);
        });
        myImage2.addEventListener('error', function () {
            console.error('Error loading image:', url2);
        });
    }
}
var imagesLoaded = 0;
var totalImages;
var allImagesLoaded;
function imgLoaded() {
    imagesLoaded++
    // console.log(imagesLoaded)
    // let pct = i * 100 / allItems.length;
    let pct = Math.round(imagesLoaded * 100 / totalImages);
    getdom("loaderPct").innerHTML = pct + " %"
    // console.log(pct)

    if (imagesLoaded >= totalImages) {
        // removeClass("loader", "show")
        imagesLoaded = 0;

        // cortina(CERRAR)
        // getdom("cortina")
        // finishLoadingAllImages() // -> ahora a actividad.js
        allImagesLoaded = true;
        finishLoadingAllAssets()
    }
}
function finishLoadingAllAssets() {
    // removeClass("loader", "show")
    if (allImagesLoaded) {
        console.log("all images loaded")
    }
    if (allSoundsLoaded) {
        console.log("allSoundsLoaded")
    }
    if (allSoundsLoaded && allImagesLoaded) {
        console.log("Finish Loading all assets")
        removeClass("loader", "show")
    }

}

async function loadWebmFile(fileUrl) {
    const response = await fetch(fileUrl);
    const blob = await response.blob();
    const objectUrl = URL.createObjectURL(blob);
    return objectUrl;
}
function playWebmFile(fileUrl) {
    const audio = new Audio();
    audio.src = fileUrl;
    audio.addEventListener('ended', () => {
        console.log('El archivo ha terminado de reproducirse');
    });
    audio.play();
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

let imagesBaseFilesUrls = [
    "ilustraciones/Personaje/exploradora_festejando.png",
    "ilustraciones/Personaje/exploradora_muestra_dibujos_cuerpo.png",
    "ilustraciones/Personaje/exploradora_pausa.png",
    "ilustraciones/Personaje/exploradora_saludando.png",
    "ilustraciones/Personaje/exploradora_senala_derecha_cuerpo.png",
    "ilustraciones/Personaje/exploradora_senala_izquierda_cuerpo.png",
    "ilustraciones/Personaje/exploradora_transicion.png",
    "ilustraciones/Personaje/exploradora_muestra_dibujos_cara.png",
    "ilustraciones/Personaje/exploradora_senala_derecha_cara.png",
    "ilustraciones/Personaje/exploradora_senala_izquierda_cara.png",

    "ilustraciones/Personaje/Expresiones/expresion1.png",
    "ilustraciones/Personaje/Expresiones/expresion2.png",
    "ilustraciones/Personaje/Expresiones/expresion3.png",
    "ilustraciones/Personaje/Expresiones/expresion4.png",
    "ilustraciones/Personaje/Expresiones/expresion5.png",
    "ilustraciones/Personaje/Expresiones/expresion6.png",
    "ilustraciones/Personaje/Expresiones/expresion7.png",
    "ilustraciones/Personaje/Expresiones/expresion8.png",
    "ilustraciones/Personaje/Expresiones/expresion9.png",
    "ilustraciones/Personaje/Expresiones/expresion10.png",
    "ilustraciones/Personaje/Expresiones/expresion11.png",
    "ilustraciones/Personaje/Expresiones/expresion12.png",
    "ilustraciones/Personaje/Expresiones/expresion13.png",
    "ilustraciones/Personaje/Expresiones/expresion14.png",
    "ilustraciones/Otros/manzana1.png",
    "ilustraciones/Otros/banana1.png",
    "ilustraciones/Fondos/fondo_1.png",
    "ilustraciones/Fondos/fondo_2_blur.png",
    "ilustraciones/Fondos/fondo_3_blur.png",
    "ilustraciones/Fondos/fondo_4.png",
    "ilustraciones/Fondos/fondo_5_blur.png",

    // "emparejamiento/dibujos/EP2_A_cebra_huele_pantera.png",
    // "emparejamiento/dibujos/EP3_A_soldado_fotografia_bombero.png",
    // "emparejamiento/dibujos/EP4_A_pintora_despeina_fotografa.png",
    // "emparejamiento/dibujos/EP5_A_perro_atrapa_canguro.png",
    // "emparejamiento/dibujos/EP2_B_pantera_huele_cebra.png",
    // "emparejamiento/dibujos/EP3_B_bombero_fotografia_soldado.png",
    // "emparejamiento/dibujos/EP4_B_fotografa_despeina_pintora.png",
    // "emparejamiento/dibujos/EP5_B_canguro_atrapa_perro.png",

    pathDibujos + "EP2_A_cebra_huele_pantera.png",
    pathDibujos + "EP3_A_soldado_fotografia_bombero.png",
    pathDibujos + "EP4_A_pintora_despeina_fotografa.png",
    pathDibujos + "EP5_A_perro_atrapa_canguro.png",
    pathDibujos + "EP2_B_pantera_huele_cebra.png",
    pathDibujos + "EP3_B_bombero_fotografia_soldado.png",
    pathDibujos + "EP4_B_fotografa_despeina_pintora.png",
    pathDibujos + "EP5_B_canguro_atrapa_perro.png",

]
async function preloadBaseFiles() {
    console.log("preload base files")
    /// IMAGES:
    let path = "/assets/"

    for (let i = 0; i < imagesBaseFilesUrls.length; i++) {
        const myImage1 = new Image(50, 50);
        let url1 = path + imagesBaseFilesUrls[i];
        myImage1.src = url1;
        myImage1.addEventListener('load', imgLoaded)
        getdom("dummyImages").appendChild(myImage1);
    }
}


let preloadPath = '../data/data/emparejamiento/dibujos/'
function testPreloadPath() {
    // let url = preloadPath + "coords_click.png"
    let url = preloadPath + "E01_A_mono_abraza_koala.png"
    console.log(url)
    document.getElementById("imgTest").src = url
}
// testPreloadPath()