
const MODO_DEBUG = false;
const iniciarEstadoEn = 0;
const ENABLE_FULLSCREEN = iniciarEstadoEn == 0 ? true : false;
let indexItemInicio = -1  // default indexItem=-1
if (MODO_DEBUG) {
    indexItemInicio = 50
}
const POSITIVO = 1;
const NEGATIVO = 2;

let practicaIndex





let epA = ["EP2_A_cebra_huele_pantera.png",
    "EP3_A_soldado_fotografia_bombero.png",
    "EP4_A_pintora_despeina_fotografa.png",
    "EP5_A_perro_atrapa_canguro.png"
]
let epB = ["EP2_B_pantera_huele_cebra.png",
    "EP3_B_bombero_fotografia_soldado.png",
    "EP4_B_fotografa_despeina_pintora.png",
    "EP5_B_canguro_atrapa_perro.png"
]
let epCorrecta = [
    "EP2_B_pantera_huele_cebra.png",
    "EP3_A_soldado_fotografia_bombero.png",
    "EP4_B_fotografa_despeina_pintora.png",
    "EP5_B_canguro_atrapa_perro.png"
]
let epIncorrecta = [
    "EP2_A_cebra_huele_pantera.png",
    "EP3_B_bombero_fotografia_soldado.png",
    "EP4_A_pintora_despeina_fotografa.png",
    "EP5_A_perro_atrapa_canguro.png"
]
let audioEp = [
    "A_la_cebra_la_huele_la_pantera.mp3",
    "El_soldado_que_fotografia_al_bombero_tiene_bigote.mp3",
    "la_pintora_a_la_que_despeina_la_fotografa_tiene_collar.mp3",
    "el_perro_es_atrapado_por_el_canguro.mp3"
]
let epEstructura = [
    "OVS",
    "RS",
    "RO",
    "Pas"
]
let epNp1 = ["cebra", "soldado", "pintora", "perro"]
let epNp2 = ["pantera", "bombero", "fotografa", "canguro"]
let epOracion = ["A la cebra la huele la pantera",
    "El soldado que fotografía al bombero tiene bigote",
    "La pintora a la que despeina la fotógrafa tiene collar",
    "El perro es atrapado por el canguro"]
let epUbicacionCorrecta = ["der", "izq", "der", "der"]
let epVerbo = ["oler", "fotografiar", "depeinar", "atrapar"]
let epRespuestas = ["res1", "res2", "res3", "res4"]

async function btnInicioActividad() {
    gIndexItem = -1;
    jugando = true;
    // siguienteItem()
    removeClass("caratulaInicioActividad", "show")
    // await cortina(CERRAR)

    presentacion_emparejamiento_start();

    addClass("btnMostrarMenuActividad", "show")

}

function presentacion_emparejamiento_start() {
    hideDuplaImagenes();

    removeClass("divImgPresentacion1", "big")
    removeClass("divImgPresentacion2", "big")

    // setImagenesPresentacion("manzana.png", "banana.png")

    addClass("pantallaPresentecionEmparejamiento", "show")

    removeClass("pantallaAdmin", "show")
    removeClass("menuContainer", "show")


    setFondoPresentacion("fondo_1.png")

    practicaIndex = 0;

    if (!MODO_DEBUG)
        activarPantallaCompleta()

    requestWakeLock();

    if (MODO_DEBUG) {
        setEstadoPresentacion(iniciarEstadoEn)
    } else {
        setEstadoPresentacion(iniciarEstadoEn)
        // setEstadoPresentacion(0)

    }
}

/////
let _animarBoca = false;
let _bocaIndex = 0;
function animarHablando(animar) {
    _animarBoca = animar
    if (_animarBoca) {
        setTimeout(function () {
            if (_animarBoca) {
                if (isPaused) {
                    setExpresion(expresionesBoca[0])
                    _animarBoca = false;

                } else {
                    setExpresion(expresionesBoca[_bocaIndex])

                    _bocaIndex++
                    if (_bocaIndex >= expresionesBoca.length) {
                        _bocaIndex = 0
                    }
                    animarHablando(_animarBoca)
                }
            }
        }, 80 + random() * 100)
        // }, 100)

    } else {
        // setPersonajePresentacionExpresion("Expresiones/expresion2.png")

        // setTimeout(function () {
        // // setPersonajePresentacionExpresion(expresionesBoca[0])
        setExpresion("Expresiones/expresion14.png")

        // }, 500)

    }
}

////////////
let expresionesOjos = [
    "exploradora_muestra_dibujos_cara.png",
    "exploradora_senala_derecha_cara.png",
    "exploradora_muestra_dibujos_cara.png",

    "exploradora_senala_izquierda_cara.png"
]
let expresionesBoca = [
    "Expresiones/expresion1.png",
    "Expresiones/expresion3.png",
    "Expresiones/expresion1.png",
    "Expresiones/expresion3.png",
    "Expresiones/expresion7.png"
]
var _animarOjos = false;
function animarOjos(animar, index = 0) {
    _animarOjos = animar;
    if (_animarOjos) {
        setExpresion(expresionesOjos[index])
        setTimeout(function () {
            index++
            if (index >= expresionesOjos.length) {
                index = 0
            }
            animarOjos(_animarOjos, index)
        }, 400)
    } else {
        // setPersonajePresentacionExpresion(expresionesOjos[index])

    }

}
/////// PERSONAJE
function setFondoPresentacion(filename) {
    let path = "/assets/ilustraciones/Fondos/"
    let url = path + filename
    getdom("fondoPresentacion").src = url
}
function setPersonaje(filename) {
    let path = "/assets/ilustraciones/Personaje/"
    let url = path + filename
    getdom("personajePresentacion").src = url
    mostrarPersonaje(true)
}
function setExpresion(filename) {
    let path = "/assets/ilustraciones/Personaje/"
    let url = path + filename
    getdom("personajePresentacionExpresion").src = url
    // mostrarPersonaje(true)
}
function mostrarPersonaje(mostrar) {
    if (mostrar) {
        addClass("personajePresentacion", "show")
        addClass("personajePresentacionExpresion", "show")
    } else {
        removeClass("personajePresentacion", "show")
        removeClass("personajePresentacionExpresion", "show")
    }
}
async function mostrarFeedback(feedback) {
    // if (feedback == false) {
    //     removeClass("divFeedback", "show")

    // } else {

    if (feedback == POSITIVO) {
        addClass("imgFeedbackPositivo", "show")
        removeClass("imgFeedbackNegativo", "show")
    } else if (feedback == NEGATIVO) {
        removeClass("imgFeedbackPositivo", "show")
        addClass("imgFeedbackNegativo", "show")
    }
    addClass("divFeedback", "show")
    // }

    await esperar(2000);
    removeClass("divFeedback", "show")
    await esperar(500)

}

//// AUDIO
const playAudioGeneral = async (filename, loop = false, _esperar = true) => {
    // let path = filename.includes("/") ? "/assets/" : "/assets/emparejamiento/audios/";
    let path = filename.includes("/") ? "/assets/" : "../data/data/emparejamiento/oraciones/";
    // let path = "../data/data/emparejamiento/oraciones/";



    let archivo = path + filename
    console.log(archivo)
    await playAudio(archivo, loop)
    if (_esperar) {

        await esperar(500)
    }
}
const personajeHablando = async (filename) => {
    animarHablando(true)
    if (!MODO_DEBUG)
        await playAudioGeneral("audios/audios_personaje/" + filename, false, false)
    animarHablando(false)
    await esperar(500)

}


//// imagenes ////////
function setDuplaImagenes(filename1, filename2) {
    // let path = "assets/ilustraciones/Otros/"
    // let path = "assets/"

    if (!filename1) {
        hideDuplaImagenes()
        return
    }

    let path = filename1.includes("/") ? "/assets/" : pathDibujos;

    playAudio("assets/audios/click.mp3")

    let url1 = path + filename1;
    let url2 = path + filename2;
    getdom("imgPresentacion1").src = url1;
    getdom("imgPresentacion2").src = url2;
    addClass("divImgPresentacion1", "show")
    addClass("divImgPresentacion2", "show")

}

function setDuplaImagenesOrdenCorrecto(filename1, filename2, item) {
    // console.log(item)
    let imagen1SinExtension = filename1.replace(".png", "");
    let imagen2SinExtension = filename2.replace(".png", "");
    // console.log(item)
    let correcta = -1;
    if (imagen1SinExtension == item.dibujo_correcto) {
        correcta = "imagen1";
    } else if (imagen2SinExtension == item.dibujo_correcto) {
        correcta = "imagen2"
    }
    let ubicacion = item.ubic_correcta
    if (correcta == "imagen1") {
        if (ubicacion == "izq") {
            setDuplaImagenes(filename1, filename2)
        } else {
            setDuplaImagenes(filename2, filename1)

        }
    } else if (correcta == "imagen2") {
        if (ubicacion == "der") {
            setDuplaImagenes(filename1, filename2)
        } else {
            setDuplaImagenes(filename2, filename1)
        }
    } else {
        alert("no coincide el nombre de las imagenes con la opcion correcta")
        return

    }
}

function hideDuplaImagenes() {
    removeClass("divImgPresentacion1", "show")
    removeClass("divImgPresentacion2", "show")
}

function showCorrecta(index) {
    if (index == false) {
        removeClass("divImgPresentacion1", "correcta")
        removeClass("divImgPresentacion2", "correcta")

    } else {
        if (index == IZQ) {
            addClass("divImgPresentacion1", "correcta")
            removeClass("divImgPresentacion2", "correcta")
        } else if (index == DER) {
            addClass("divImgPresentacion2", "correcta")
            removeClass("divImgPresentacion1", "correcta")
        }
    }
}

async function showCortina(show) {
    if (show) {
        addClass("cortinaGral", "show")
    } else {
        if (!isPaused)
            removeClass("cortinaGral", "show")
    }
    await esperar(200)
}

//// CLICK
function clickEnImagenPresentacion(index) {
    // console.log(index)
    if (estadoActual == 2) {
        if (index == IZQ) {
            /// incorrecto
            setEstadoPresentacion(3)
        } else {
            setEstadoPresentacion(4)
        }
    }
    else if (estadoActual == 9) {
        if (index == DER) {
            /// incorrecto
            setEstadoPresentacion(10)
        } else {
            setEstadoPresentacion(11)
        }
    } else if (estadoActual == 201) {
        // console.log("practica index:", practicaIndex)
        // console.log(getCorrectaPractica(practicaIndex))
        if (!hablandoEnEsperaDeAccion) {

            if (index == getCorrectaPractica(practicaIndex)) {
                setEstadoPresentacion(202)
            } else {
                setEstadoPresentacion(203)
            }

        }
    } else if (estadoActual > 300 && estadoActual < 400) {
        clickEnImagenEstados300(index)
    }
}

function esperar(_time) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(); // Resolvemos la promesa cuando la tarea está completa
        }, _time);
    });
}

