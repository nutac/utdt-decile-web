
**Pesta♦a configuracion y datos**

    PONER PASSWORD



**HACER**

* [ ] sonidos

AGREGAR BOTON DE PAUSAA

* [ ] cortina traslucida
* [ ] que al exportar el archivo de los registros:
  * [ ] expoerte en un csv (los datos diferentes son nuevas columnas)
  * [ ] agregar estos campos:
    * [ ] nombre
    * [ ] fecha
  * [ ] en pesta;a "nuevo" agregar campo "grupo"
* [ ] en tiempo de respuesta usar milisegundos en lugar de segundos
* [X] pantalla finalizacion -> guardar registro en BD
* [X] menu -> Nuevo (crear), opciones, registro

* *** dar feedback visual cuando se hace click sobre una imagen y que se pause durante 1 segundo***

* [ ] usar variable "tiempo de espera"
* [X] campo -> recordatorios -> 0/1/2
* [X] campo fecha
* [ ]
* [X] subir un CSV a firebase
* [X] modelo de datos coleccion:
  * [X] id
  * [X] nombre_coleccion
  * [X] path_imagenes
  * [X] items[array]
* [X] poder ponerle nombre a esa coleccion
* [X] poder listar las colecciones de firebase desde el cliente
* [ ] poder eliminar colecciones desde el cliente?
* [X] al listar las colecciones, poder seleccionar una y tomar esa para cargar los datos (poblar la lista allItems? o cambiar de nombre)
* [ ] poder setear el path de las imagenes

**DB REQs**

* cargar TSV con items
* guardar datos del participante
* exportar datos del participante
* recortar las imagenes con codigo para que queden cuadradas

**HACER**

* pasar a html (reemplazar img src)
* importar tsv
* boton menu
* boton oculto para cancelar la exp.
* **CONSULTAR TTS de google o cómo conseguir el tono de pregunta**
* poder grabar los datos antes de terminar la experiencia (para no perder datos)

**INICIO**

    Input:

* Cod. participante
* seleccion de lista
* colegio/institucion

**PROCESO**

* randomizar orden de trials
* al iniciar el trial
  * suena el audio con la oracion
  * no mostrar imagen hasta q termine el audio de la oracion
* que el touch no funcione si toca en la zona de frontera entre imagenes

**FINAL**

Output:

* cod participante
* lista
* colegio/inst
* fecha
* datos de cada trial:
  * set
  * estructura
  * nombre_imagen_1
  * nombre_imagen_2
  * verbo
  * NP1
  * NP2
  * oracion
  * ...
  * (todos los campos del excel y ademas:)
    * nro de orden (random)
    * tiempo
    * respuesta correcta/incorrecta
