let tiempoEsperaDeAccion = 2000; // 3 segundos
let contadorDeEspera = 0;

let contadorRespuestasErroneasEnPractica = 0


async function setEstadoPresentacion2(estado) {
    if (estado >= 300) {
        setEstadoPresentacion3(estado)
        return
    }
    console.log("E " + estadoActual)

    switch (estado) {
        case 200:
            /// INICIO LOOP PRACTICAS!!! ///////////////////////////////////
            clearTimeout(timeoutId);

            setDuplaImagenes(epA[practicaIndex], epB[practicaIndex])
            await esperar(1000);

            await showCortina(true)
            // await esperar(1000);
            await playAudioGeneral(audioEp[practicaIndex])
            // await esperar(500);
            
            if (practicaIndex == 0) {
                await esperar(500);
                await personajeHablando("Practica2_toca_el_dibujo_correcto.mp3")
            }
            await showCortina(false)

            contadorRespuestasErroneasEnPractica = 0
            setEstadoPresentacion(201);
            break;

        case 201:
            /// espera de accion
            manejarEsperaDeAccion();
            break
        case 202:
            /// CORRECTO
            await mostrarFeedback(POSITIVO)
            await siguienteItemDePractica()

            break;
        case 203:
            /// INCORRECTO
            await mostrarFeedback(NEGATIVO)
            contadorDeEspera = 0;
            if (contadorRespuestasErroneasEnPractica == 0) {
                await showCortina(true)
                await personajeHablando("Practica3_intentalo_de_vuelta.mp3")
                await showCortina(false)

            } else if (contadorRespuestasErroneasEnPractica == 1) {
                showCorrecta(getCorrectaPractica(practicaIndex))
                await personajeHablando("Practica4_este_es_el_dibujo_correcto.mp3")
                await esperar(1000)
                showCorrecta(false)

                await esperar(500);
                await siguienteItemDePractica()
                return

                ///  ///////////// hacer titilar al dibujo correcto
            }
            //  else if (contadorRespuestasErroneasEnPractica == 2) {
            //     await personajeHablando(".mp3")
            // }
            await esperar(500);
            contadorRespuestasErroneasEnPractica++

            setEstadoPresentacion(201);
            break;
        // case 204:
        //     /// 
        //     setEstadoPresentacion(SIGUIENTE);
        //     break;
        // case 204:
        //     setEstadoPresentacion(SIGUIENTE);
        //     break;
        // case 21:

        //     break;
        default:
            console.log("ESTADO INEXISTENTE");
            break;
    }
}

let timeoutId;
let hablandoEnEsperaDeAccion = false
async function manejarEsperaDeAccion() {
    contadorDeEspera++;
    hablandoEnEsperaDeAccion = false


    // Usamos setTimeout
    timeoutId = setTimeout(async () => {
        if (estadoActual == 201) { // Si el estado sigue siendo 16, entonces no ha habido ninguna acción del usuario
            hablandoEnEsperaDeAccion = true
            switch (contadorDeEspera) {

                case 1:
                    console.log("mensaje 1");
                    await showCortina(true)
                    await personajeHablando("Practica5_escucha_de_nuevo.mp3")
                    await esperar(500)
                    await playAudioGeneral(audioEp[practicaIndex])
                    await showCortina(false)
                    manejarEsperaDeAccion();

                    break;
                case 2:
                    console.log("mensaje 2");
                    await showCortina(true)
                    await playAudio("assets/audios/silbido_llamado_atencion.mp3")
                    await personajeHablando("Practica6_estas_ahi_Toca_a.mp3")
                    await personajeHablando("Practica6_estas_ahi_Toca_b.mp3")
                    await showCortina(false)

                    manejarEsperaDeAccion(); // Volvemos a esperar
                    break;
                case 3:
                    console.log("mensaje 3");

                    showCorrecta(getCorrectaPractica(practicaIndex))
                    // showCorrecta(IZQ)
                    await personajeHablando("Practica7_a.mp3")
                    if (practicaIndex < epA.length - 1) {
                        await personajeHablando("Practica7_b.mp3")
                    }
                    showCorrecta(false)
                    await esperar(1000)

                    await siguienteItemDePractica()
                    break;
            }
        }
    }, tiempoEsperaDeAccion);
}

function getCorrectaPractica(_index2) {
    let _index = practicaIndex
    let res
    if (_index == 0) {
        res = DER
    } else if (_index == 1) {
        res = IZQ
    } else if (_index == 2) {
        res = DER
    } else if (_index == 3) {
        res = DER
    }
    return res
}

async function siguienteItemDePractica() {
    guardarRegistroPractica()
    practicaIndex++
    contadorDeEspera = 0;
    // console.log(gRegistro)
    if (practicaIndex == epA.length) {
        // fin de la practica
        console.log("FIN DE LA PRACTICA")
        setEstadoPresentacion(300)
    } else {
        // siguiente item
        setEstadoPresentacion(200);
    }
}

function guardarRegistroPractica() {
    let i = practicaIndex
    let orden = 0;
    if (gRegistro.items.length > 0) orden = gRegistro.items.length
    let newItem = {
        dibujo_correcto: epCorrecta[i],
        dibujo_incorrecto: epIncorrecta[i],
        estructura: epEstructura[i],
        imagen1: epA[i],
        imagen2: epB[i],
        lista: "practica",
        np1: epNp1[i],
        np2: epNp2[i],
        oracion: epOracion[i],
        set: "p",
        ubic_correcta: epUbicacionCorrecta[i],
        verbo: epVerbo[i],
        respuesta: epRespuestas[i],
        recordatorios: contadorDeEspera,
        tiempoDeRespuesta: 0,
        orden: orden
    }
    gRegistro.items.push(newItem)
    console.log(gRegistro.items)

}