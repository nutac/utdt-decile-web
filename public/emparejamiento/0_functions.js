function getdom(_control) {
    let element;
    if (typeof _control === "string" || _control instanceof String) {
        element = document.getElementById(_control);
    } else {
        element = _control;
    }
    return element;
}

function addClass(element, class_name) {
    getdom(element).classList.add(class_name)
}

function removeClass(element, class_name) {
    getdom(element).classList.remove(class_name)
}

