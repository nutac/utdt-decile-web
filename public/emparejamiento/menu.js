window.addEventListener("load", function () {
    clickMenuItem(0)
})
function clickMenuItem(_index) {
    let menuItems = document.getElementsByClassName("menuItem")
    let tabs = document.getElementsByClassName("tab")

    // if (_index == 1) {
    //     // presentacion_emparejamiento_start();
    //     console.log("hola")
    //     window.location.href = '/'
    //     return
    // }
    // console.log(_index)
    if (_index == 2) {
        getdom("pass").value = ''
        if (!MODO_DEBUG)
            addClass("containerPassword", "show")

    }
    for (let i = 0; i < menuItems.length; i++) {
        if (i == _index) {
            addClass(menuItems[i], "selected")
            addClass(tabs[i], "show")
        } else {
            removeClass(menuItems[i], "selected")
            removeClass(tabs[i], "show")
        }
    }
}
var escapeCounter = 0;
const escapeMax = 3;
function btnEscapeClick(_index) {
    console.log(_index)
    if (_index == 0) {
        escapeCounter++
        if (escapeCounter >= escapeMax) {
            escapeCounter = 0
            addClass("btnEscape1", "show")
        } else {
            removeClass("btnEscape1", "show")

        }
    } else {
        removeClass("btnEscape0", "show")
        removeClass("btnEscape1", "show")

        removeClass("pantallaActividad", "show")
        addClass("pantallaAdmin", "show")
        addClass("menuContainer", "show")
    }
}

/////////////////////////////////////////////////
let timeoutHiddenButton;
getdom(btnMostrarMenuActividad).addEventListener('contextmenu', (e) => {
    e.preventDefault();
});
getdom(btnMostrarMenuActividad).addEventListener('mousedown', (e) => {
    timeoutHiddenButton = setTimeout(() => {
        pausarActividad(true);
        // console.log("mostrar menu")
    }, 2000);
})
getdom(btnMostrarMenuActividad).addEventListener('touchstart', (e) => {
    timeoutHiddenButton = setTimeout(() => {
        pausarActividad(true);
        // console.log("mostrar menu touch")
    }, 2000);
})

getdom(btnMostrarMenuActividad).addEventListener('mouseup', (e) => {
    clearTimeout(timeoutHiddenButton);
})
getdom(btnMostrarMenuActividad).addEventListener('touchend', (e) => {
    clearTimeout(timeoutHiddenButton);

})

////////////////////////////////////////////////////////////////
/// HIDEN MENU
getdom("btnVolverAlInicio").addEventListener('click', () => {
    // console.log("volver al inicio")
    reiniciarYVolverAlInicio();
})

getdom("btnGuardarYVolverAlInicio").addEventListener('click', () => {
    // guardarRegistroItem();

    /// ejecuta: fb.setNewRegistro()
    getdom("dummyBtnSaveRegistroDb").click()
    reiniciarYVolverAlInicio();

})
getdom("btnFullscreen").addEventListener('click', () => {
    if (isInFullScreen()) {
        activarPantallaCompleta(false, true);
    } else {
        activarPantallaCompleta(true, true);

    }
})

getdom("btnContinuarDesdePausa").onclick = () => {
    pausarActividad(false)
}

///////////////////////////////////////////////////////

document.addEventListener("visibilitychange", function () {
    if (document.hidden) {
        // console.log("La pantalla podría estar bloqueada o la app en segundo plano");
        pausarActividad(true);

    } else {
        // console.log("La pantalla está activa y la app en primer plano");
        // pausarActividad(false)

    }
});
