// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.18.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, child, get, set, push, orderByChild, query, equalTo, remove } from "https://www.gstatic.com/firebasejs/9.18.0/firebase-database.js"

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBMmYgZfwcy4kMdRZ8nH_zEfsHNOCh8koU",
    authDomain: "picr-40137.firebaseapp.com",
    databaseURL: "https://picr-40137-default-rtdb.firebaseio.com",
    projectId: "picr-40137",
    storageBucket: "picr-40137.appspot.com",
    messagingSenderId: "946678339287",
    appId: "1:946678339287:web:7529255941295a774f0292"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getDatabase(app);
const dbRef = ref(db);

getRegistros()

export async function getTiempo() {
    return await get(child(dbRef, "utdt/tiempo/")).then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val();
        }
    })

}
export async function setTiempo(tiempo) {
    return set(ref(db, 'utdt/tiempo'),
        tiempo
    )
        .then(() => {
            return true
        })
        .catch((error) => {
            console.log("hubo un error:", error)
            return false
        });
}

export async function getRegistros() {
    // let listaHTML = "";
    await get(child(dbRef, "utdt/registros")).then((snapshot) => {
        if (snapshot.exists()) {

            // console.log(JSON.stringify(snapshot.val()))
            const elementos = snapshot.val();
            arrayRegistros = []
            for (const key in elementos) {
                if (elementos.hasOwnProperty(key)) {
                    // listaHTML += `<li>${elementos[key]}</li>`;

                    // console.log("-------------")
                    // console.log(key, elementos[key])
                    // console.log("-------------")
                    let registro = elementos[key];
                    let fila = {}
                    for (const campo in registro) {
                        // console.log(campo, registro[campo])
                        // if (campo != "items") {
                        // fila.push(registro[campo])
                        fila[campo] = registro[campo]
                        // }
                    }
                    arrayRegistros.push(fila)

                }
            }
            // console.log(arrayRegistros)
            // poblarTablaRegistros(arrayRegistros)
            poblarTablaRegistros()

            // document.getElementById("lista-elementos").innerHTML = listaHTML;
        } else {
            console.log("No data available");
        }
    }).catch((error) => {
        console.error(error);
    });
}

// getDataTest()
export async function getListas() {
    let resultado;
    await get(child(dbRef, "utdt/listas")).then((snapshot) => {
        if (snapshot.exists()) {
            // console.log(snapshot.val());
            resultado = snapshot.val();
        } else {
            console.log("No data available");
        }
    }).catch((error) => {
        console.error(error);
    });
    return resultado;
}




export function setNewRegistro() {
    // setTimeout(function () {
    //     console.log(gRegistro)
    //     // removeClass("guardandoDatos", "show")
    //     // addClass("btnVolver", "show")
    //     finDelGuardadoDeRegistro()
    // }, 2000)

    push(ref(db, 'utdt/registros/'),
        // _data
        gRegistro
    )
        .then(() => {
            // Data saved successfully!
            console.log("Registro guardado en BD!")
            // return true
            finDelGuardadoDeRegistro()
            getRegistros()
        })
        .catch((error) => {
            console.log("hubo un error con Firebase:", error)
            // The write failed...
            alert("Error: " + error)
        });

}

//////////////////////////////
// export async function appendListasToDatabase() {
//     if (allItems != undefined) {
//         addClass("loader", "show")
//         getdom("comboListas").innerHTML = ""
//         await setDataIntoLists(allItems)
//             .then(function () {
//                 requestListas()
//                 removeClass("loader", "show")
//             })
//     }
// }

export async function setDataIntoLists(_items) {
    // console.log("allItems: ", allItems)
    let data = {};
    _items.forEach((item) => {
        // console.log(item.lista, item)
        if (!data[item.lista]) {
            data[item.lista] = [];
        }
        data[item.lista].push(item);
    });
    // console.log(data)
    for (let key in data) {
        // console.log(key);
        console.log(key, data[key])
        await setData(key, data[key])
    }
    return true
}
export async function setData(_folderName, _data) {
    return await set(ref(db, 'utdt/listas/' + _folderName),
        _data
    )
        .then(() => {
            // Data saved successfully!
            // console.log("Todo OK!")
            return true
        })
        .catch((error) => {
            console.log("hubo un error:", error)
            return false
            // The write failed...
        });
}


async function deleteRegistroByFecha(targetFecha) {
    const registrosRef = child(dbRef, "utdt/registros");
    get(registrosRef).then((snapshot) => {
        if (snapshot.exists()) {
            const registros = snapshot.val();
            for (let key in registros) {
                if (registros[key].fecha === targetFecha) {
                    // Eliminar el registro y terminar la búsqueda
                    remove(child(registrosRef, key)).then(() => {
                        console.log("Registro eliminado con éxito.");
                        getRegistros()
                    }).catch((error) => {
                        console.error("Error eliminando el registro:", error);
                    });
                    return;
                }
            }
            console.log("No se encontró registro con la fecha especificada.");
        } else {
            console.log("No hay registros.");
        }
    }).catch((error) => {
        console.error("Error obteniendo los registros:", error);
    });
}

window.deleteRegistroByFecha = deleteRegistroByFecha;
