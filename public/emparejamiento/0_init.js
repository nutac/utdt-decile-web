import * as fb from "./0_bd_firebase.js"
let version = 1029
document.getElementById('version-tag').innerHTML += version;


// window.onerror = function (message, source, lineno, colno, error) {
//     // alert(`Error ocurrido: ${message} en ${source}:${lineno}:${colno}`);
//     // Si quieres detener la propagación del error al manejo de errores por defecto del navegador:
//     // alert(`${source}:${lineno} -> ${message}`);
//     console.log(`${source}:${lineno} -> ${message}`);

//     return true;
// };
// window.onunhandledrejection = function (event) {
//     console.log(`Error no manejado en promesa: ${event.reason}`);
// };
// document.addEventListener('error', function (event) {
//     if (event.target.tagName === 'IMG') {
//         // console.log(event)
//         // alert(`Error: ${event.target.src}`);
//         // console.error(event.target)
//         console.log(`${source}:${lineno} -> ${message}`);

//     } else if (event.target.tagName === 'SCRIPT') {
//         console.log(`Error al cargar script: ${event.target.src}`);
//     } else if (event.target.tagName === 'LINK' && event.target.rel === 'stylesheet') {
//         console.log(`Error al cargar hoja de estilo: ${event.target.href}`);
//     }
//     // ... Agrega aquí otros tipos de recursos si los necesitas.
// }, true);


///////////////////////
// requestListas();
requestTiempo();

function requestListas() {
    console.log("Updating Listas...")
    // loadData()
    // addClass("loader", "show")

    fb.getListas().then((res) => {
        gListas = res;
        console.log(gListas)
        populateComboListas(res)
        removeClass("loader", "show")
    })
}

requestListaEnCSV()
function requestListaEnCSV() {
    parseCSVFromPath("../data/data/emparejamiento/emparejamiento.csv")
        .then(res => {
            gListas = res;
            console.log(gListas)
            populateComboListas(res)
            removeClass("loader", "show")
        });

}

async function parseCSVFromPath(path) {
    try {
        const response = await fetch(path);
        const text = await response.text();
        const lines = text.trim().split('\n');
        const headers = lines[0].split(',');

        const data = {};

        for (let i = 1; i < lines.length; i++) {
            const row = lines[i].split(',');
            // console.log(row)

            const item = headers.reduce((obj, header, index) => {
                obj[header.trim()] = row[index].trim();
                return obj;
            }, {});

            // Ignorar los elementos que tengan el valor "ignorar" en el campo Lista
            if (item["Lista"] === "ignorar") continue;

            // Crear el objeto con la estructura deseada
            const structuredItem = {
                dibujo_correcto: item["Dibujo_correcto"],
                dibujo_incorrecto: item["Dibujo_incorrecto"],
                estructura: item["Estructura"],
                imagen1: item["Nombre_dibujo_A"] + ".png",
                imagen2: item["Nombre_dibujo_B"] + ".png",
                lista: item["Lista"],
                np1: item["NP1"],
                np2: item["NP2"],
                oracion: item["Oración"],
                set: item["Set"],
                ubic_correcta: item["Ubic_corr"],
                verbo: item["Verbo"]
            };

            // Añadir el objeto al grupo correspondiente, creando el grupo si no existe
            if (!data[item["Lista"]]) {
                data[item["Lista"]] = [];
            }
            data[item["Lista"]].push(structuredItem);
        }

        // console.log(data); // Mostrar el resultado en la consola
        return data;

    } catch (error) {
        console.error("Error al cargar el archivo CSV:", error);
    }
}







function requestTiempo() {
    fb.getTiempo().then((tiempo) => {
        getdom("inpTiempo").value = tiempo
        console.log("tiempo:", tiempo);
    })
}
function populateComboListas(_listas) {
    // console.log(_listas)
    const opciones = [];
    const filtroOpciones = ["*"];

    for (let key in _listas) {
        opciones.push(key);
        filtroOpciones.push(key);
    }

    const combo = getdom("comboListas");
    const filtroCombo = getdom("filtroListas");

    let opcionesHTML = "";
    let filtroOpcionesHTML = "";

    for (let i = 0; i < opciones.length; i++) {
        opcionesHTML += `<option value="${opciones[i]}">${opciones[i]}</option>`;
    }

    for (let i = 0; i < filtroOpciones.length; i++) {
        filtroOpcionesHTML += `<option value="${filtroOpciones[i]}">${filtroOpciones[i]}</option>`;
    }

    combo.innerHTML = opcionesHTML;
    filtroCombo.innerHTML = filtroOpcionesHTML;
}

/////////////////////////////////
// getdom("btnTest").addEventListener("click", function () {
//     test();
// })

async function test() {
    console.log("test!")


    // if (allItems != undefined) {
    //     addClass("loader", "show")
    //     //     loadDataFromCsv()
    //     // } else {
    //     //     // console.log(allItems)
    //     getdom("comboListas").innerHTML = ""
    //     await fb.setDataIntoLists(allItems)
    //         .then(function () {
    //             // console.log("LISTO!")
    //             requestListas()
    //             removeClass("loader", "show")
    //         })
    // }

    // checkIfDone()

    // function checkIfDone() {
    //     setTimeout(function () {
    //         let html = getdom("comboListas").innerHTML
    //         if (html == "") {
    //             console.log("Html vacio:", html)
    //             checkIfDone()
    //         } else {
    //             console.log("Html:", html)
    //         }
    //     },
    //         1000)
    // }
}

function loadDataFromCsv() {
    // addClass("loader", "show")

    const csvFile = csvFileInput.files[0];

    if (!csvFile) {
        console.log("seleccionar archivo csv!!")
        return
    }
    // loadStrings('assets/test.csv', finishLoadingStrings)
    loadStrings(csvFile, finishLoadingStrings)

    function finishLoadingStrings(_resultingArray) {
        createItemsFromCsvArray(_resultingArray)
        // loadImages(allItems)


        fb.setDataIntoLists(allItems)

    }
}


/// DUMMYS PARA USAR IMPORT

getdom("dummyBtnSaveRegistroDb").addEventListener("click", function () {
    fb.setNewRegistro()
})

getdom("dummyBtnSaveTiempoDb").addEventListener("click", function () {
    let tiempo = getdom("inpTiempo").value
    fb.setTiempo(tiempo)
})

getdom("btnDummyAppendListToDb").addEventListener("click", async function () {
    // fb.appendListasToDatabase()
    if (allItems != undefined) {
        addClass("loader", "show")
        getdom("comboListas").innerHTML = ""
        await fb.setDataIntoLists(allItems)
            .then(function () {
                requestListas()
                removeClass("loader", "show")
            })
    }
})
