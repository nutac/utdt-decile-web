// var archivo;
var allItems;
// let csv;
const _SET = 0;
const _LISTA = 1;
const _ESTRUCTURA = 2;
const _DIBUJO1 = 3;
const _DIBUJO2 = 4;
const _VERBO = 5;
const _NP1 = 6;
const _NP2 = 7;
const _ORACION = 8
const _UBIC_CORR = 9;

const _DIBUJO_CORRECTO = 10
const _DIBUJO_INCORRECTO = 11

// function preload() {
//     // archivo = loadJSON('items.json')
//     // csv = loadStrings('assets/emparejamiento.csv')
// }
function setup() {
    // readCsv()
    // loadImages()
}
// function draw() {
// }


function createItemsFromCsvArray(_csvArray) {
    allItems = []
    console.log(_csvArray)
    for (let i = 1; i < _csvArray.length; i++) {
        const rowArray = splitTokens(_csvArray[i], ",")
        // let lista = rowArray[_LISTA]
        // // console.log(lista)
        // // console.log(i, rowArray[1], rowArray[2], rowArray[3])
        // if (lista == 'a') {
        // console.log(rowArray.length)
        if (rowArray.length > 0) {
            pushItem(i, rowArray)
        }
        // }
        // // if (i > 9) break;
        // if (allItems.length >= 20) break
    }
    console.log(allItems)
    // console.log(JSON.stringify(allItems))
}
function pushItem(_id, _row) {
    let item = {
        // id: _id,
        set: _row[_SET],
        lista: _row[_LISTA],
        estructura: _row[_ESTRUCTURA],
        imagen1: _row[_DIBUJO1] + ".png",
        imagen2: _row[_DIBUJO2] + ".png",
        verbo: _row[_VERBO],
        np1: _row[_NP1],
        np2: _row[_NP2],
        oracion: _row[_ORACION],
        ubic_correcta: _row[_UBIC_CORR],
        dibujo_correcto: _row[_DIBUJO_CORRECTO],
        dibujo_incorrecto: _row[_DIBUJO_INCORRECTO],

    }
    allItems.push(item)
}
