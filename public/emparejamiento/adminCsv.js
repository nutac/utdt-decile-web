




// function parseCsv(text, filterValue) {
//     const lines = text.trim().split('\r\n');
//     const headers = lines.shift().split(',');
//     const results = [];

//     lines.forEach(line => {
//         const row = line.split(',');
//         const obj = {};

//         headers.forEach((header, index) => {
//             obj[header.toLowerCase()] = row[index];
//         });

//         // if (obj['Lista'] === filterValue) {
//         results.push(obj);
//         // }
//     });

//     // console.log(results[0].Lista)

//     return results;
// }



function guardarArrayEnCSV(array, nombreArchivo) {
    // Convertir el array a una cadena de texto CSV
    const csv = array.map(row => row.join(',')).join('\n');

    // Crear un objeto Blob a partir de la cadena de texto CSV
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });

    // Crear un objeto URL a partir del objeto Blob
    const url = URL.createObjectURL(blob);

    // Crear un enlace de descarga en el DOM y configurarlo para descargar el archivo CSV
    const link = document.createElement('a');
    link.setAttribute('href', url);
    link.setAttribute('download', `${nombreArchivo}.csv`);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
// function exportToCsv(data) {
//     const csvContent = "data:text/csv;charset=utf-8," +
//         data.map(row => Object.values(row).join(",")).join("\n");

//     const encodedUri = encodeURI(csvContent);
//     const link = document.createElement("a");
//     link.setAttribute("href", encodedUri);
//     link.setAttribute("download", "prueba.csv");
//     document.body.appendChild(link);
//     link.click();
//     document.body.removeChild(link);
// }
function downloadCsv(resultCsv) {
    const headers = Object.keys(resultCsv[0]).join(","); // Obtener los headers del objeto
    const csvRows = resultCsv.map(row =>
        Object.values(row).join(",")
    ); // Mapear el objeto a un arreglo de filas CSV
    const csvString = [headers, ...csvRows].join("\n"); // Concatenar los headers y filas en un solo string

    const a = document.createElement("a"); // Crear un elemento <a> para descargar el archivo
    const file = new Blob([csvString], { type: "text/csv" }); // Crear un objeto Blob con el contenido CSV
    a.href = URL.createObjectURL(file); // Establecer el objeto Blob como la URL de descarga del <a>
    a.download = "result.csv"; // Establecer el nombre del archivo de descarga
    document.body.appendChild(a); // Agregar el <a> al DOM
    a.click(); // Simular el click en el <a> para descargar el archivo
    document.body.removeChild(a); // Eliminar el <a> del DOM
}

// btnSaveCsv.addEventListener("click", () => {
//     console.log("csvToBeSaved:", csvToBeSaved)

//     // guardarArrayEnCSV(csvToBeSaved, "prueba.csv")
//     // exportToCsv(csvToBeSaved)
//     downloadCsv(csvToBeSaved)
// })

function filterData(csvResult, fieldName, filterValue) {
    // let result = [];
    // for (let i = 0; i < csv.length; i++) {
    //     // if (i > 10) break

    //     const line = csv[i];
    //     result.push(line)
    //     // console.log(line)
    // }

    // return result;
    return csvResult.filter(row => row[fieldName] === filterValue);

}
function filterCsvResult(csvResult, fieldName, filterValue) {
    return csvResult.filter(row => row[fieldName] === filterValue);
}

