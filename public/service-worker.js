const CACHE_NAME = 'my-app-cache-v2.158';
const urlsToCache = [
    '/assets/',
    // Puedes añadir aquí otros recursos que quieras cachear inicialmente, como CSS, JS, etc.
];

self.addEventListener('install', event => {
    // event.waitUntil(
    //     caches.open(CACHE_NAME)
    //         .then(cache => {
    //             console.log('Opened cache');
    //             return cache.addAll(urlsToCache);
    //         })
    // );

    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.filter(cacheName => {
                    return cacheName !== CACHE_NAME;
                }).map(cacheName => {
                    return caches.delete(cacheName);
                })
            );
        })
    );

    // self.skipWaiting();
});

self.addEventListener('fetch', event => {
    const requestUrl = new URL(event.request.url);

    // if (requestUrl.pathname.includes('mp3')) {
    //     console.log('Solicitud:', requestUrl.pathname);
    // }

    // Solo manejar solicitudes que se dirigen a la carpeta 'assets/'
    // console.log(requestUrl)
    if (requestUrl.pathname.startsWith('/assets/')) {
        event.respondWith(
            caches.match(event.request)
                .then(response => {
                    // Devuelve la respuesta del caché si existe
                    if (response) {
                        // console.log("ya existe: ", response.url)
                        return response;
                    }
                    return fetch(event.request).then(
                        response => {
                            // Comprueba si recibimos una respuesta válida
                            if (!response || response.status !== 200 || response.type !== 'basic') {
                            // if (!response || response.status !== 200) {
                                console.log("res no valida: ", response.status, response.url)
                                return response;
                            }
                            const responseToCache = response.clone();
                            caches.open(CACHE_NAME)
                                .then(cache => {
                                    console.log("guardando: ", event.request.url)
                                    cache.put(event.request, responseToCache);
                                });
                            return response;
                        }
                    );
                })
        );
    } else {
        // console.log("no cacheado:", requestUrl)
    }

});
